<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Curriculo extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'curriculo_emprego';
    protected $fillable = [
        'naturalidade', 'idade','endereco','celular','objetivo','qualificacoes','escolaridade','info_add','id_usuario'
    ];

    public function Experiencia(){
        return $this->hasMany('App\Experiencia');
    }

    public function Formacao(){
        return $this->hasMany('App\Formacao');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
