<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Emprego extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'divulgar_emprego';
    protected $fillable = [
        'tipo_cont','nome_contratante', 'email_empresa', 'bairro','contato','carga_horaria','data','descricao','status','cargo',
        'estado','cidade','id_usuario'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
