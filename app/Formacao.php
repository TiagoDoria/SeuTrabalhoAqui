<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Formacao extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'formacao';
    protected $fillable = [
        'instituicao','curso','semestre','ano_conclusao_previsao','id_usuario'
    ];

    public function Curriculo(){
        return $this->belongsTo('App\Curriculo');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
