<?php

namespace App\Http\Controllers;
use app\Http\Requests;
use App\User;
use App\Trabalho;
use App\Curriculo;
use App\Experiencia;
use App\Formacao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class AdminController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function listar_usuarios(){
    $users = DB::table('users')
    ->select('users.*')->where('users.admin',0)->orderBy('name')->paginate(50);

    return view('Admin.listar_usuarios')->with('users',$users);
  }

  public function index(){
    $users = DB::table('users')
    ->select('users.*')->get();
    $ctd_users = count($users);
    $vagas = DB::table('divulgar_emprego')
    ->select('divulgar_emprego.*')->get();
    $ctd_vagas = count($vagas);
    return View('Admin.home')->with('ctd_users',$ctd_users)->with('ctd_vagas',$ctd_vagas);
  }

  public function editar_usuario($id){
    $perfil_usuario = DB::table('users')
    ->select('users.*')->where('id',$id)
    ->get();
    return view('Admin.editar_usuario')->with('perfil_usuario',$perfil_usuario);
  }

  public function update_usuario(Request $request,$id){

      $data = [
        'name'=>$request->name
        ,'email'=>$request->email
        ,'cpf'=>$request->cpf
        ,'bairro'=> $request->bairro
        ,'cidade'=> $request->cidade
        ,'estado'=> $request->estado
        ,'pais'=> $request->pais
       ,'sexo'=> $request->sexo];

        $user = DB::table('users')
                ->select('users.*')->where('id',$id)
                ->update($data);

        return Redirect('/admin');

  }

  public function excluir_usuario($id){
    $form = DB::table('users')
    ->select('users.* ')->where('users.id',$id)
    ->delete();

    return Redirect('/admin');
  }

  public function listar_vagas(){
    $vagas = DB::table('divulgar_emprego')
    ->select('divulgar_emprego.*')->orderBy('nome_contratante')->paginate(50);

    return view('Admin.listar_vagas_emp')->with('vagas',$vagas);
  }

  public function editar_vagas_emp($id){
    $vaga_emp = DB::table('divulgar_emprego')
    ->select('divulgar_emprego.*')->where('id',$id)
    ->get();
    return view('Admin.editar_vaga_emp')->with('vaga_emp',$vaga_emp);
  }

  public function update_vagas_emp(Request $request,$id){
    $data = [
      'nome_contratante'=>$request->nome_contratante
      ,'area_atuacao'=>$request->area_atuacao
      ,'cargo'=>$request->cargo
      ,'carga_horaria'=>$request->carga_horaria
      ,'bairro'=> $request->bairro
      ,'cidade'=> $request->cidade
      ,'estado'=> $request->estado
      ,'beneficios'=> $request->beneficios
      ,'salario'=> $request->salario
     ,'descricao'=> $request->descricao];

      $user = DB::table('divulgar_emprego')
              ->select('divulgar_emprego.*')->where('id',$id)
              ->update($data);

      return Redirect('/admin');
  }

  public function excluir_vaga_emp($id){
    $form = DB::table('divulgar_emprego')
    ->select('divulgar_emprego.* ')->where('divulgar_emprego.id',$id)
    ->delete();

    return Redirect('/admin');
  }

  public function pesquisar_usuario(Request $request){

    $users = User::where(function($query) use($request) {

         if($request->has('name')){
             $name = $request->name;
             $query->where('name', "like", "%{$name}%");
            }

         if($request->has('email')){

               $email = $request->email;
               $query->where('email', "like", "%{$email}%");
           }
          if($request->has('cpf')){

             $cpf = $request->cpf;
             $query->where('cpf', "like", "%{$cpf}%");
           }

    })
    ->paginate(10);
    $count = count($users);
    if(count($users) != 0)
      return view('Admin.listar_usuarios')->with('users',$users)->with('count',$count);
    else {
        \Session::flash('flash_message_error','Nenhum usuário encontrado!!');
        return Redirect('listar_usuarios')->with('message','Nenhum usuário encontrado!!');
    }
  }

  public function pesquisar_vaga_emp(Request $request){

    $vagas = Trabalho::where(function($query) use($request) {

         if($request->has('nome_contratante')){
             $nome_contratante = $request->nome_contratante;
             $query->where('nome_contratante', "like", "%{$nome_contratante}%");
            }

         if($request->has('cargo')){

               $cargo = $request->cargo;
               $query->where('cargo', "like", "%{$cargo}%");
           }
          if($request->has('cidade')){

             $cidade = $request->cidade;
             $query->where('cidade', "like", "%{$cidade}%");
           }

    })
    ->paginate(10);
    $count = count($vagas);
    if(count($vagas) != 0)
      return view('Admin.listar_vagas_emp')->with('vagas',$vagas)->with('count',$count);
    else {
        \Session::flash('flash_message_error','Nenhuma vaga encontrada!!');
        return Redirect('listar_vagas_emp')->with('message','Nenhum usuário encontrado!!');
    }
  }


}
