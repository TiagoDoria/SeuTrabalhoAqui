<?php

namespace App\Http\Controllers;
use app\Http\Requests;
use App\Trabalho;
use App\Experiencia;
use App\Formacao;
use App\Curriculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use PDF;
class HomeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index2()
    {
      if(!empty(Auth()->user()->id))
        return view('home');
      else {
        return view('errologin');
      }
    }

    public function visualizar($id){

      $trabs = DB::table('divulgar_emprego')
      ->select('divulgar_emprego.*')->where('divulgar_emprego.id',$id)
      ->get();

       return view('trabalho.tview', compact('trabs'));
    }


      public function index(){
        $cont = DB::table('divulgar_emprego')->count('*');
        $trabalho = DB::table('divulgar_emprego')
        ->select('divulgar_emprego.*')->paginate(5);


        return View('index')->with('trabalho',$trabalho)->with('cont',$cont);
    }

    public function contato()
    {
        return view('contato');
    }

    public function envio(Request $request){


      $vetor = [
          'nome' => $request->nome,
          'email' => $request->email,
          'cidade'=>$request->cidade,
          'mensagem' => $request->mensagem,

        ];
        Mail::send('mail_contato', $vetor, function($message) use ($vetor) {
           $message->to('seutrabalhoaqui2017@gmail.com', 'Contato')->subject
              ('Contato SeuTrabalhoAqui');
           $message->from($vetor['email'],'Contato');
        });
        return Redirect('/');
    }

    public function listar(){
      $count = DB::table('divulgar_emprego')->count('*');
      $trabalho = DB::table('divulgar_emprego')
      ->select('divulgar_emprego.*')->paginate(10);
      $cargo = null;
      return View('trabalho.listar_emp')->with('trabalho',$trabalho)->with('count',$count)->with('cargo',$cargo);
    }

    public function pesquisar_emp(Request $request){

      $trabalho = Trabalho::where(function($query) use($request) {
           $cargo = 1;
           if($request->has('area_atuacao')){
               $area_atuacao = $request->area_atuacao;
               $query->where('area_atuacao', "like", "%{$area_atuacao}%");
           }

           if($request->has('bairro')){

                 $bairro = $request->bairro;
                 $query->where('bairro', "like", "%{$bairro}%");
           }

          if($request->has('cidade')){

                 $cidade = $request->cidade;
                 $query->where('cidade', "like", "%{$cidade}%");
          }


          if($request->has('estado')){

                 $estado = $request->estado;
                 $query->where('estado', "like", "%{$estado}%");
             }
      })
      ->paginate(10);
      $count = count($trabalho);
      if(count($trabalho) != 0)
        return view('trabalho.listar_emp')->with('trabalho',$trabalho)->with('count',$count)->with('cidade',$request->cidade)->with('estado',$request->estado)->with('bairro',$request->bairro)->with('area_atuacao',$request->area_atuacao);
      else {
          \Session::flash('flash_message_error','Nenhuma vaga encontrada!!');
          return Redirect('listar_emp')->with('message','Nenhuma vaga encontrada com essas características!!');
      }
    }


    public function search_index(Request $request){
       $cargo = $request->cargo_index;
       $estado = $request->estado_index;

       $trabalho = DB::table('divulgar_emprego')
       ->select('divulgar_emprego.*')->where('cargo',$cargo)->where('estado',$estado)
       ->paginate(10);

       $count = count($trabalho);
       if(count($trabalho) != 0)
         return view('trabalho.listar_emp')->with('trabalho',$trabalho)->with('count',$count);
       else {
           \Session::flash('flash_message_error','Nenhuma vaga encontrada!!');
           return Redirect('/')->with('message','Nenhuma vaga encontrada!!');
       }

    }
}
