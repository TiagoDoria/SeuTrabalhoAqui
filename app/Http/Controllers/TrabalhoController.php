<?php

namespace App\Http\Controllers;
use app\Http\Requests;
use App\Trabalho;
use App\Experiencia;
use App\Formacao;
use App\User;
use App\Candidatura;
use App\Curriculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use PDF;

class TrabalhoController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }


  public function dados_pessoais_emp(){
    return View('trabalho.curriculo_trabalho');
  }

  public function formacao_emprego(){
    return View('trabalho.addformacao_emprego');
  }

  public function experiencia_emprego(){
    return View('trabalho.addexperiencia_emprego');
  }

  public function addpessoais(Request $request)
  {
    $verifica = DB::table('curriculo_emprego')
    ->select('curriculo_emprego.*')->where('id_usuario',Auth()->user()->id)
    ->get();
    $count = count($verifica);

    if($count >= 1){
      \Session::flash('flash_message_error','Currículo já cadastrado!');
      return redirect('/home');
    }
    else{
      $pessoal = new Curriculo;
      $pessoal->naturalidade = $request->naturalidade;
      $pessoal->idade = $request->idade;
      $pessoal->endereco = $request->endereco;
      $pessoal->celular = $request->celular;
      $pessoal->objetivo = $request->objetivo;
      $pessoal->qualificacoes = $request->desc_qua;
      $pessoal->info_add = $request->info_add;
  
      $pessoal->id_usuario = Auth()->user()->id;
      if(is_numeric($pessoal->idade))
        $pessoal->save();
      else{
        \Session::flash('flash_message_error','Erro! Dados inválidos!');
        return redirect('/home');
      }

      return redirect('/home')->with('message','Cadastrado com sucesso!');
  }
  }

  public function addexpr(Request $request)
  {
    $expr = new Experiencia;
    $expr->empresa = $request->empresa;
    $expr->periodo = $request->periodo;
    $expr->desc_atv = $request->desc_atv;
    $expr->id_usuario = Auth()->user()->id;
    $expr->save();

    return redirect('/home')->with('message','Cadastrado com sucesso!');

  }

  public function addform(Request $request)
  {
    $form = new Formacao;
    $form->instituicao = $request->instituicao;
    $form->curso = $request->curso;
    $form->semestre = $request->semestre;
    $form->escolaridade = $request->escolaridade;
    $form->ano_conclusao_previsao = $request->previsao;
    $form->id_usuario = Auth()->user()->id;
    $form->save();

    return redirect('/home')->with('message','Cadastrado com sucesso!');

  }

  public function createpdf()
	{
    $curriculo = DB::table('users')
    ->select('users.*')->where('id',Auth()->user()->id)
    ->first();

    $curriculop = DB::table('curriculo_emprego')
    ->select('curriculo_emprego.*')->where('id_usuario',Auth()->user()->id)
    ->first();
    if(!$curriculop){
      \Session::flash('flash_message_error','Currículo não cadastrado!');
      return Redirect ('/home');}


    $formacao = DB::table('formacao')
    ->select('formacao.*')->where('id_usuario',Auth()->user()->id)
    ->get();

    if(count($formacao)==0){
      \Session::flash('flash_message_error','Formação acadêmica não cadastrada!');
      return Redirect ('/home');}

    $expr = DB::table('experiencias')
    ->select('experiencias.*')->where('id_usuario',Auth()->user()->id)
    ->get();

    $pdf = PDF::loadView('trabalho.meu_curriculo',compact('curriculo','curriculop','formacao','expr'));
    return $pdf->stream();
	}

  public function visualizar_curriculo($id)
  {
    $curriculo = DB::table('users')
    ->select('users.*')->where('id',decrypt($id))
    ->first();

    $curriculop = DB::table('curriculo_emprego')
    ->select('curriculo_emprego.*')->where('id_usuario',Auth()->user()->id)
    ->first();
    if(!$curriculop){
      \Session::flash('flash_message_error','Currículo não cadastrado!');
      return Redirect ('/home');}
   
    $formacao = DB::table('formacao')
    ->select('formacao.*')->where('id_usuario',decrypt($id))
    ->get();

    if(count($formacao)==0){
      \Session::flash('flash_message_error','Curriculo do usuário está inválido!!');
      return Redirect ('/vagas_ofertadas');}

    $expr = DB::table('experiencias')
    ->select('experiencias.*')->where('id_usuario',decrypt($id))
    ->get();

    $pdf = PDF::loadView('trabalho.meu_curriculo',compact('curriculo','curriculop','formacao','expr'));
    return $pdf->stream();
  }

  public function editar(){
    $pessoais = DB::table('curriculo_emprego')
    ->select('curriculo_emprego.*')->where('curriculo_emprego.id_usuario',Auth()->user()->id)
    ->get();

    $expr = DB::table('experiencias')
    ->select('experiencias.*')->where('experiencias.id_usuario',Auth()->user()->id)
    ->get();

    $formacao = DB::table('formacao')
    ->select('formacao.*')->where('formacao.id_usuario',Auth()->user()->id)
    ->get();

    return view('trabalho.editar_curriculo',compact('pessoais','expr','formacao'));
  }

  public function update(Request $request,$id){

    $data = [
      'naturalidade'=>$request->naturalidade
      ,'idade'=>$request->idade
      ,'endereco'=>$request->endereco
      ,'celular'=> $request->celular
     
      ,'qualificacoes'=> $request->desc_qua
      ,'info_add'=> $request->info_add];

      $cont = DB::table('experiencias')->select('experiencias.*')->where('experiencias.id_usuario',decrypt($id))->count('*');
      $cont2 = DB::table('formacao')->select('formacao.*')->where('formacao.id_usuario',decrypt($id))->count('*');
      for($i=0;$i<$cont;$i++){
      $id_expr=$request->id_expr[$i];
      $data2 = [
        'empresa'=>$request->empresa[$i]
        ,'periodo'=>$request->periodo[$i]
        ,'desc_atv'=>$request->desc_atv[$i]
        ,'id_usuario'=>Auth()->user()->id
       ];

       $expr = DB::table('experiencias')
               ->select('experiencias.*')->where('experiencias.id_usuario',decrypt($id))->where('experiencias.id',$id_expr)
               ->update($data2);
      }
      for($j=0;$j<$cont2;$j++){
       $id_form=$request->id_form[$j];
       $data3 = [
         'escolaridade'=> $request->escolaridade
         ,'instituicao'=>$request->instituicao[$j]
         ,'curso'=>$request->curso[$j]
         ,'semestre'=>$request->semestre[$j]
         ,'ano_conclusao_previsao'=>$request->previsao[$j]
         ,'id_usuario'=>Auth()->user()->id
        ];

      $formacao = DB::table('formacao')
              ->select('formacao.*')->where('formacao.id_usuario',decrypt($id))->where('formacao.id',$id_form)
              ->update($data3);
      }
      $pessoais = DB::table('curriculo_emprego')
              ->select('curriculo_emprego.*')->where('curriculo_emprego.id_usuario',decrypt($id))
              ->update($data);

      return Redirect('/home');
    }

    public function excluir_expr($id2){

      $expr = DB::table('experiencias')
      ->select('experiencias.* ')->where('experiencias.id',$id2)
      ->delete();


      return Redirect('editar_curriculo/');

    }

     public function candidatos($id){

      $pend = DB::table('candidatura')
      ->join('users','candidatura.id_usuario','=','users.id')
      ->join('divulgar_emprego','candidatura.id_divulgar','=','divulgar_emprego.id')
      ->select('divulgar_emprego.*','users.*','candidatura.*')
      ->where('divulgar_emprego.id',$id)
      ->where('candidatura.status','Pendente')
      ->get();

      $aprov = DB::table('candidatura')
      ->join('users','candidatura.id_usuario','=','users.id')
      ->join('divulgar_emprego','candidatura.id_divulgar','=','divulgar_emprego.id')
      ->select('divulgar_emprego.*','users.*','candidatura.*')
      ->where('divulgar_emprego.id',$id)
      ->where('candidatura.status','Aprovado')
      ->get();

      $repr = DB::table('candidatura')
      ->join('users','candidatura.id_usuario','=','users.id')
      ->join('divulgar_emprego','candidatura.id_divulgar','=','divulgar_emprego.id')
      ->select('divulgar_emprego.*','users.*','candidatura.*')
      ->where('divulgar_emprego.id',$id)
      ->where('candidatura.status','Reprovado')
      ->get();

      return view('trabalho.candidatos',compact('pend','repr','aprov'));

  }

  public function approve_candidate($id,$id2){
      $data = [
          'status'=>'Aprovado'
         
      ];    
       
      $pessoais = DB::table('candidatura')
      ->select('candidatura.status')
      ->where('id_usuario',decrypt($id))
      ->where('id_divulgar',$id2)
      ->update($data);

      return Redirect('candidatos/'. $id2);
  }

  public function disapprove_candidate($id,$id2){
      $data = [
          'status'=>'Reprovado'
         
      ];    
       
      $pessoais = DB::table('candidatura')
      ->select('candidatura.status')
      ->where('id_usuario',decrypt($id))
      ->where('id_divulgar',$id2)
      ->update($data);

      return Redirect('candidatos/'. $id2);
  }


  public function excluir_form($id2){

    $form = DB::table('formacao')
    ->select('formacao.* ')->where('formacao.id',$id2)
    ->delete();


    return Redirect('editar_curriculo/');

  }


    public function candidatar($id2,$id3){

      $user = DB::table('users')
      ->select('users.*')->where('users.id',Auth()->user()->id)
      ->first();

      $curriculo = DB::table('curriculo_emprego')
      ->select('curriculo_emprego.*')->where('curriculo_emprego.id_usuario',Auth()->user()->id)
      ->get();

      $ctd = count($curriculo);

      $contratante = DB::table('users')
      ->select('users.*')->where('users.id',decrypt($id2))
      ->first();

      $vaga = DB::table('divulgar_emprego')
      ->select('divulgar_emprego.*')->where('divulgar_emprego.id',decrypt($id3))
      ->first();

      if($ctd != 0){

        $candidatar = new Candidatura();
        $candidatar->id_usuario = Auth()->user()->id;
        $candidatar->id_divulgar = $vaga->id;
        $candidatar->score = 0;
        $candidatar->save();


      }  

      if($ctd != 0){

       
        $vetor = [
            'id' => $user->id,
            'nome' => $user->name,
            'mailc' => $contratante->email,
            'vaga'=>$vaga->cargo,
            'descricao'=>$vaga->descricao,
            'emailto'=>$vaga->email_empresa,
            'mensagem' => "Olá!  $user->name acaba de se candidatar a uma vaga disponibilizada pelo(a) senhor(a)! Entre em contato para agendar uma entrevista!",

          ];
          Mail::send('mail_candidato', $vetor, function($message) use ($vetor) {
             $message->to($vetor['emailto'], 'Candidatura')->subject
                ('Parabéns! Mais uma pessoa interessada');
             $message->from($vetor['mailc'],'Candidatura');
          });
          \Session::flash('flash_message_sucess','Candidatura efetuada com sucesso!!');
          return Redirect('ver_vaga/'.decrypt($id3));
        }
        else{
          \Session::flash('flash_message_error','Currículo não cadastrado!!');
          return Redirect('ver_vaga/'.decrypt($id3));
        }
    }

    public function download_curriculo($id){
      $curriculo = DB::table('users')
      ->select('users.*')->where('id',decrypt($id))
      ->first();

      $curriculop = DB::table('curriculo_emprego')
      ->select('curriculo_emprego.*')->where('id_usuario',decrypt($id))
      ->first();

      $formacao = DB::table('formacao')
      ->select('formacao.*')->where('id_usuario',decrypt($id))
      ->get();

      $expr = DB::table('experiencias')
      ->select('experiencias.*')->where('id_usuario',decrypt($id))
      ->get();

      $pdf = PDF::loadView('trabalho.meu_curriculo',compact('curriculo','curriculop','formacao','expr'));
      return $pdf->download();
    }

    public function vagas_ofertadas(){
      $vagas = DB::table('divulgar_emprego')
      ->select('divulgar_emprego.*')->where('divulgar_emprego.id_usuario',Auth()->user()->id)
      ->paginate(10);

      return view('ofertadas')->with('vagas',$vagas);
    }

    public function editar_vaga_ofer($id){
      $vaga_emp = DB::table('divulgar_emprego')
      ->select('divulgar_emprego.*')->where('id',$id)->where('id_usuario',Auth()->user()->id)
      ->get();
      return view('editar_vaga_ofer')->with('vaga_emp',$vaga_emp);
    }

    public function update_vaga_ofer(Request $request,$id){


        $data = [
          'nome_contratante'=>$request->nome_contratante
          ,'tipo_cont'=>$request->tipo_cont
          ,'cargo'=> $request->cargo
          ,'carga_horaria'=>$request->carga_horaria
          ,'bairro'=> $request->bairro
          ,'cidade'=> $request->cidade
          ,'estado'=> $request->estado
          ,'salario'=> $request->salario
          ,'turno'=> $request->turno
          ,'beneficios'=> $request->beneficios
          ,'descricao'=> $request->descricao];


          $pessoais = DB::table('divulgar_emprego')
                  ->select('divulgar_emprego.*')->where('id',$id)
                  ->update($data);

          return Redirect('/vagas_ofertadas');

    }

    public function excluir_vaga_ofer($id){
      $form = DB::table('divulgar_emprego')
      ->select('divulgar_emprego.* ')->where('divulgar_emprego.id',$id)
      ->delete();

      return Redirect('/vagas_ofertadas');
    }

    public function pesquisar_vaga_ofer(Request $request){

      $vagas = Trabalho::where(function($query) use($request) {

           if($request->has('area_atuacao')){
               $area_atuacao = $request->area_atuacao;
               $query->where('area_atuacao', "like", "%{$area_atuacao}%");
           }

           if($request->has('bairro')){

                 $cargo = $request->bairro;
                 $query->where('bairro', "like", "%{$bairro}%");
           }

          if($request->has('cidade')){

                 $cargo = $request->cidade;
                 $query->where('cidade', "like", "%{$cidade}%");
          }


          if($request->has('estado')){

                 $cargo = $request->estado;
                 $query->where('estado', "like", "%{$estado}%");
             }


      })->where('id_usuario',Auth()->user()->id)
      ->paginate(10);
      $count = count($vagas);
      if(count($vagas) != 0)
        return view('ofertadas')->with('vagas',$vagas)->with('count',$count);
      else {
          \Session::flash('flash_message_error','Nenhuma vaga encontrada!!');
          return Redirect('vagas_ofertadas')->with('message','Nenhuma vaga encontrado!!');
      }
    }


}
