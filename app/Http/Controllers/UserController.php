<?php

namespace App\Http\Controllers;
use app\Http\Requests;
use App\User;
use App\Estagio;
use App\Temporario;
use App\Trabalho;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function divulgar_emprego()
  {
      return view('divulgar_emprego');
  }

  public function cadastro_oportunidade(Request $request)
  {
    $divulgar = new Trabalho;
    $estagio = new Estagio;
    $temporario = new Temporario;
   
    if(is_numeric($request->carga_horaria)){
      $divulgar->nome_contratante = $request->nome_empresa;
      $divulgar->email_empresa = $request->email_empresa;
      $divulgar->tipo_cont = $request->tipo_cont;
      $divulgar->cargo = $request->cargo;
      $divulgar->carga_horaria = $request->carga_horaria;
      $divulgar->turno = $request->turno;
      $divulgar->bairro = $request->bairro;
      $divulgar->cidade = $request->cidade;
      $divulgar->estado = $request->estado;
      if($request->salario)$divulgar->salario = $request->salario; else $divulgar->salario = 0;
      if($request->beneficios)  $divulgar->beneficios = $request->beneficios;
      if($request->pre_requisitos)  $divulgar->pre_requisitos = $request->pre_requisitos;
      $divulgar->descricao = $request->descricao;
      $divulgar->id_usuario = Auth()->user()->id;

      if($request->tipo_cont == 'clt') $divulgar->save();

      if($request->tipo_cont == 'estagio'){  
        $lastValue = DB::table('divulgar_emprego')->orderBy('id', 'desc')->first();
        $estagio->semestre_i = $request->semestre_i;
        $estagio->semestre_f = $request->semestre_f;
        $estagio->formacao = $request->form_cursos;
        $estagio->id_vaga = $lastValue->id + 1;
        if($request->form_cursos && is_numeric($request->semestre_i) && is_numeric($request->semestre_f)){
          $divulgar->save();
          $estagio->save();
        }
      }

      if($request->tipo_cont == 'temporario'){
        $lastValue = DB::table('divulgar_emprego')->orderBy('id', 'desc')->first();
        $temporario->duracao = $request->duracao;
        $temporario->id_vaga = $lastValue->id + 1;
        $divulgar->save();
        $temporario->save();
      }




      return redirect('/')->with('message','Oportunidade cadastrada com sucesso!');
   }
   else{
     \Session::flash('flash_message_error','Erro! Dados inválidos!');
     return redirect('/home');
   }

  }

  public function perfil($id){
    $perfil = DB::table('users')
    ->select('users.*')->where('id',$id)
    ->get();
    return view('Perfil.perfil')->with('perfil',$perfil);
  }

  public function meu_perfil(){
    $perfil = DB::table('users')
    ->select('users.*')->where('id',Auth()->user()->id)
    ->get();

    return view('Perfil.meu_perfil')->with('perfil',$perfil);
  }

  public function editar_perfil(){
    $meuperfil = DB::table('users')
    ->select('users.*')->where('id',Auth()->user()->id)
    ->get();
    return view('Perfil.editar')->with('meuperfil',$meuperfil);
  }

  public function update_perfil(Request $request){

    $data = [
      'name'=>$request->name
      ,'email'=>$request->email
      ,'cpf'=>$request->cpf
      ,'bairro'=> $request->bairro
      ,'cidade'=> $request->cidade
      ,'estado'=> $request->estado
      ,'pais'=> $request->pais
     ,'sexo'=> $request->sexo];


      $pessoais = DB::table('users')
              ->select('users.*')->where('id',Auth()->user()->id)
              ->update($data);

      return Redirect('/home');
    }

    public function denunciar_usuario($id){
      $user = DB::table('users')
      ->select('users.*')->where('id',decrypt($id))
      ->get();

      return view('Perfil.denunciar')->with('user',$user);
    }


}
