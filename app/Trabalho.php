<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Trabalho extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'divulgar_emprego';
    protected $fillable = [
        'tipo_cont','nome_contratante','email_empresa','cargo','carga_horaria','bairro','cidade','estado','salario','beneficios','descricao','turno','status','id_usuario'
    ];

    public function Candidatura(){
      return $this->hasMany('App\Candidatura');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
