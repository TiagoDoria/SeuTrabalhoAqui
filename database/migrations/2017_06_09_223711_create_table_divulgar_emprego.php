<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDivulgarEmprego extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divulgar_emprego', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_contratante');
            $table->string('tipo_cont');
            $table->string('email_empresa');
            $table->string('cargo');
            $table->string('turno');
            $table->string('carga_horaria');
            $table->string('bairro');
            $table->string('cidade');
            $table->string('estado');
            //$table->double('salario');
            $table->string('descricao');
            $table->string('pre_requisitos');
            $table->string('status');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('users');

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divulgar_emprego');
    }
}
