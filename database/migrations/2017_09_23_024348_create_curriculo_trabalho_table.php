<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurriculoTrabalhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('curriculo_emprego', function (Blueprint $table) {
          $table->increments('id');
          $table->string('naturalidade');
          $table->integer('idade');
          $table->string('endereco');
          $table->string('celular');
          $table->string('objetivo');
          $table->string('qualificacoes');
          $table->string('info_add');
          $table->integer('id_usuario')->unsigned();
          $table->foreign('id_usuario')->references('id')->on('users');

          $table->timestamps();


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculo_emprego');
    }
}
