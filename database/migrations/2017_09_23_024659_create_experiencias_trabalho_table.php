<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienciasTrabalhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('experiencias', function (Blueprint $table) {
          $table->increments('id');
          $table->string('empresa');
          $table->string('periodo');
          $table->string('desc_atv');
          $table->integer('id_curriculo_emp')->unsigned();
          $table->foreign('id_curriculo_emp')->references('id')->on('curriculo_emprego');
          $table->integer('id_usuario')->unsigned();
          $table->foreign('id_usuario')->references('id')->on('users');

          $table->timestamps();


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiencias');
    }
}
