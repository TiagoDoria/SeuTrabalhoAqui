<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('formacao', function (Blueprint $table) {
          $table->increments('id');
          $table->string('escolaridade');
          $table->string('instituicao');
          $table->string('curso');
          $table->integer('semestre');
          $table->integer('previsao');
         
          $table->integer('id_usuario')->unsigned();
          $table->foreign('id_usuario')->references('id')->on('users');

          $table->timestamps();


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formacao');
    }
}
