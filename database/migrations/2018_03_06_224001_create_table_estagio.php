<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEstagio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estagio', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('semestre_i');
          $table->integer('semestre_f');
          $table->string('formacao');
          $table->integer('id_vaga')->unsigned();
          $table->foreign('id_vaga')->references('id')->on('divulgar_emprego');
          $table->timestamps();


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
