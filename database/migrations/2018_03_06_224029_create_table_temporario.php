<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTemporario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporario', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('duracao');
          $table->integer('id_vaga')->unsigned();
          $table->foreign('id_vaga')->references('id')->on('divulgar_emprego');
          $table->timestamps();


      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
