<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="">
  <meta name="author" content="">
  <title>Seu Trabalho é Aqui</title>

  <!-- Bootstrap core CSS -->
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- Plugin CSS -->
  <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="/css/sta-admin.css" rel="stylesheet">

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  @if(Session::has('flash_message_error'))
  <div class="text-center alert alert-danger"><span  class="text-center glyphicon glyphicon-ok"></span><em> {!! session('flash_message_error') !!}</em></div>
  @endif

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="/">SeuTrabalhoAqui</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="/home">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">
              Página inicial</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text">
                Usuários </span>
              </a>
              <ul class="sidenav-second-level collapse" id="collapseComponents">

                <li>
                  <a href="{{route('listar_usuarios')}}">1) Listar</a>
                </li>

              </ul>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
              <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#escolha" data-parent="#exampleAccordion">
                <i class="fa fa-fw fa-wrench"></i>
                <span class="nav-link-text">
                  Vagas </span>
                </a>
                <ul class="sidenav-second-level collapse" id="escolha">

                  <li>
                    <p class="text-center" style="color:#fff;" data-toggle="collapse">Emprego</p></li>
                    <li>
                      <a href="{{ route('listar_vagas_emp')}}">1.1) Listar</a>
                    </li>


                  
                    </ul>
                  </li>
                      <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
                        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#minhasvagas" data-parent="#exampleAccordion">
                          <i class="fa fa-fw fa-sitemap"></i>
                          <span class="nav-link-text">
                            Outros</span>
                          </a>
                          <ul class="sidenav-second-level collapse" id="minhasvagas">
                            <li>
                              <a href="#">Denúncias</a>
                            </li>
                            <li>
                              <a href="#">...</a>
                            </li>
                            <li>
                              <a href="#">...</a>
                            </li>
                          </ul>
                        </li>

                      </ul>
                      <ul class="navbar-nav sidenav-toggler">
                        <li class="nav-item">
                          <a class="nav-link text-center" id="sidenavToggler">
                            <i class="fa fa-fw fa-angle-left"></i>
                          </a>
                        </li>
                      </ul>
                      <ul class="navbar-nav ml-auto">
                        <li style="color:#fff;padding:6px;" class="nav-item">Bem vindo,{{ Auth::user()->name }} ( <i class="fa fa-user-circle" aria-hidden="true"></i> <a href="{{ route('meu_perfil',['id'=>Auth::user()->id])}}"> Ver perfil </a>)</li>
                        <li class="nav-item">
                          <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">

                          <i class="fa fa-fw fa-sign-out"></i>
                          Sair</a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                          </form>
                        </li>
                      </ul>
                    </div>
                  </nav>

                  <div class="content-wrapper">

                    <div class="container-fluid">

                      <!-- Breadcrumbs -->
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="#">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Página principal</li>
                      </ol>

                      <div class="row">

                        <div class="col-md-8">
                          @foreach ($perfil_usuario as $perfil)
                          <form class="myform" method="POST" action="{{ route('update_usuario',['id'=> $perfil->id])}}">
                              {{ csrf_field() }}

                              <div class="form-group form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                  <label style="font-weight:bolder;" for="name" class="col-md-4 control-label">Nome</label>

                                  <div class="form-group">
                                      <input id="name" type="text" class="form-control" name="name" value="{{$perfil->name}}" required autofocus>

                                      @if ($errors->has('name'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('name') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>

                              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                  <label style="font-weight:bolder;" for="email" class="col-md-4 control-label">E-mail</label>

                                  <div class="form-group">
                                      <input id="email" type="text" class="form-control" name="email" value="{{$perfil->email}}" required>

                                      @if ($errors->has('email'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('email') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                  <label style="font-weight:bolder;" for="cpf" class="col-md-4 control-label">CPF</label>

                                  <div class="form-group">
                                      <input id="cpf" type="text" class="form-control" name="cpf" value="{{$perfil->cpf}}" required>

                                      @if ($errors->has('cpf'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('cpf') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="form-group{{ $errors->has('bairro') ? ' has-error' : '' }}">
                                  <label style="font-weight:bolder;" for="bairro" class="col-md-4 control-label">Bairro</label>

                                  <div class="form-group">
                                      <input id="bairro" type="text" class="form-control" name="bairro" value="{{ $perfil->bairro}}" required>

                                      @if ($errors->has('bairro'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('bairro') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>

                              <div class="form-group{{ $errors->has('cidade') ? ' has-error' : '' }}">
                                  <label style="font-weight:bolder;" for="cidade" class="col-md-4 control-label">Cidade</label>

                                  <div class="form-group">
                                      <input id="cidade" type="text" class="form-control" name="cidade" value="{{$perfil->cidade }}" required>

                                      @if ($errors->has('cidade'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('cidade') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>


                              <div class="form-group{{ $errors->has('estado') ? ' has-error' : '' }}">
                                  <label style="font-weight:bolder;" for="estado" class="col-md-4 control-label">Estado</label>

                                  <div class="form-group">
                                      <input id="estado" type="text" class="form-control" name="estado" value="{{ $perfil->estado }}" required>

                                      @if ($errors->has('estado'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('estado') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>

                              <div class="form-group{{ $errors->has('pais') ? ' has-error' : '' }}">
                                  <label style="font-weight:bolder;" for="pais" class="col-md-4 control-label">País</label>

                                  <div class="form-group">
                                      <input id="pais" type="text"  class="form-control" name="pais" value="{{ $perfil->pais }}">

                                      @if ($errors->has('pais'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('pais') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>

                              <div class="form-group{{ $errors->has('sexo') ? ' has-error' : '' }}">
                                  <label style="font-weight:bolder;" for="sexo" class="col-md-4 control-label">Sexo</label>

                                  <div class="form-group">
                                      <input id="sexo" type="text" class="form-control" name="sexo" value="{{ $perfil->sexo }}" required>

                                      @if ($errors->has('sexo'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('sexo') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>

                              <div class="form-group">
                                  <div class="form-group">
                                      <button type="submit" class="btn btn-primary">
                                          Editar
                                      </button>
                                  </div>
                              </div>
                              @endforeach
                          </form>

                          </div>


                        </div>
                        <!-- /.container-fluid -->

                      </div>
                      <!-- /.content-wrapper -->


                      <footer class="sticky-footer">
                        <div class="container">
                          <div class="text-center">
                            <small>Copyright &copy; seutrabalhoaqui 2017</small>
                          </div>
                        </div>
                      </footer>

                      <!-- Scroll to Top Button -->
                      <a class="scroll-to-top rounded" href="#page-top">
                        <i class="fa fa-angle-up"></i>
                      </a>


                      <!-- Bootstrap core JavaScript -->
                      <script src="/vendor/jquery/jquery.min.js"></script>
                      <script src="/vendor/popper/popper.min.js"></script>
                      <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

                      <!-- Plugin JavaScript -->
                      <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
                      <script src="/vendor/chart.js/Chart.min.js"></script>
                      <script src="/vendor/datatables/jquery.dataTables.js"></script>
                      <script src="/vendor/datatables/dataTables.bootstrap4.js"></script>

                      <!-- Custom scripts for this template -->
                      <script src="js/sta-admin.min.js"></script>

                    </body>

                    </html>
