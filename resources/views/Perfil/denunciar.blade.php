<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Seu Trabalho é Aqui</title>
    <!-- Bootstrap core CSS -->
   <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <!-- Custom fonts for this template -->
   <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
   <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>

   <link href="/css/grayscale.min.css" rel="stylesheet">
   <link href="/css/styles.css" rel="stylesheet">

   <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  </head>
<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">Sobre</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#buscar">Buscar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#divulgar">Divulgar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <section id="contato" class="content-section text-center">
    <div class="container">
      <div class="col-md-12">
        <div class="">
          <br><br>
          <h3 class="text-center">Página de denúncia</h3><br>
          <p class="text-center">.</p><br>
          @foreach($user as $users)
          <form  action="{{ route('envio') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-6 text-center">
                <label style="font-weight:bolder;">Nome do usuário a ser denunciado: *</label>
                <input type="text" class="form-control" value="{{$users->name}}"  disabled>
              </div>
              <div class="col-md-6 form-group{{ $errors->has('motivo') ? ' has-error' : '' }}">
                  <label style="font-weight:bolder;" for="motivo" class="col-md-12 control-label">Motivo*</label>

                  <div class="form-group">

                      <select class="form-group form-control col-md-12" name="motivo" required aria-required="true">
                      <option value="">Escolha</option>
                      <option value="preconceito">Postagens preconceituosas</option>
                      <option value="vagasfalsas">Vagas falsas</option>
                      <option value="conteudoimproprio">Conteúdo impróprio</option>
                      <option value="perfilfalso">Perfil falso</option>
                      <option value="outros">Outros(Especifique abaixo)</option>

                      </select>

                      @if ($errors->has('motivo'))
                          <span class="help-block">
                              <strong>{{ $errors->first('motivo') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>
            <br>

            <div class="form-group col-md-12">
              <label for="comment" style="font-weight:bolder;">Informações adicionais</label>
              <textarea class="form-control" rows="10" id="infomais" name="infomais" required></textarea>
            </div>
            <button type="submit" class="btn btn-primary text-center">Denunciar</button>
            @endforeach
          </form>
        </div>
      </div>
    </div>


  </section>




  <!-- Footer -->
  <footer style="background-color:#000;">
    <div class="container text-center">
      <p style="color:#fff;">Copyright &copy; seutrabalhoaqui 2017</p>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/popper/popper.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

  <!-- Custom scripts for this template -->
  <script src="/js/sta.min.js"></script>

</body>


</html>
