@extends('layouts.app')

@section('content')
 <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<body id="page-top">
  <!-- Navigation -->
 <!-- Navigation -->
  @if(empty(Auth()->user()->id))
      <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
          <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fa fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="/">Home</a>
              </li>
                  
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    @else
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="/">Home</a>
            </li>
          
           
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>

            </li>

            <li style="color:#fff; padding: 2px;" class="nav-item">
               <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents">
              <i class="material-icons">account_circle</i>&nbsp; <?php $nome = explode(" ", Auth()->user()->name); echo $nome[0]; ?>
               </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents">
                  <li><a href="{{ route('home') }}">Entrar</a></li>
                  <li><a href="{{ route('meu_perfil') }}">Perfil</a></li>
                  <li> <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">Sair</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form></li>     
                </ul>
              </li>

          </ul>
        </div>
      </div>
    </nav>
    @endif
<div class="container">
  <br><br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <h2 class="text-center">Editar perfil</h2>
                <br><br>
                <div class="panel-body">
                    <form class="myform" method="POST" action="{{ route('update_perfil') }}">
                        {{ csrf_field() }}
                        @foreach ($meuperfil as $perfil)
                        <div class="form-group form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nome</label>

                            <div class="form-group">
                                <input id="name" type="text" class="form-control" name="name" value="{{$perfil->name}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-mail</label>

                            <div class="form-group">
                                <input id="email" type="text" class="form-control" name="email" value="{{$perfil->email}}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="cpf" class="col-md-4 control-label">CPF</label>

                            <div class="form-group">
                                <input id="cpf" type="text" class="form-control" name="cpf" value="{{$perfil->cpf}}" required>

                                @if ($errors->has('cpf'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cpf') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('bairro') ? ' has-error' : '' }}">
                            <label for="bairro" class="col-md-4 control-label">Bairro</label>

                            <div class="form-group">
                                <input id="bairro" type="text" class="form-control" name="bairro" value="{{ $perfil->bairro}}" required>

                                @if ($errors->has('bairro'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bairro') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('cidade') ? ' has-error' : '' }}">
                            <label for="cidade" class="col-md-4 control-label">Cidade</label>

                            <div class="form-group">
                                <input id="cidade" type="text" class="form-control" name="cidade" value="{{$perfil->cidade }}" required>

                                @if ($errors->has('cidade'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cidade') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('estado') ? ' has-error' : '' }}">
                            <label for="estado" class="col-md-4 control-label">Estado</label>

                            <div class="form-group">
                                <input id="estado" type="text" class="form-control" name="estado" value="{{ $perfil->estado }}" required>

                                @if ($errors->has('estado'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('estado') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pais') ? ' has-error' : '' }}">
                            <label for="pais" class="col-md-4 control-label">País</label>

                            <div class="form-group">
                                <input id="pais" type="text"  class="form-control" name="pais" value="{{ $perfil->pais }}">

                                @if ($errors->has('pais'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pais') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sexo') ? ' has-error' : '' }}">
                            <label for="sexo" class="col-md-4 control-label">Sexo</label>

                            <div class="form-group">
                                <input id="sexo" type="text" class="form-control" name="sexo" value="{{ $perfil->sexo }}" required>

                                @if ($errors->has('sexo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sexo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Editar
                                </button>
                            </div>
                        </div>
                        @endforeach
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="" style="background-color:#000;">
  <div class=" text-center">
    <p style="color:#fff;">Copyright &copy; seutrabalhoaqui 2017</p>
  </div>
</footer>
@endsection
