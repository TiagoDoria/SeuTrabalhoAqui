<?php $aux = 1; ?>  <div class="form-group form-group{{ $errors->has('name_empresa') ? ' has-error' : '' }}"> <label for="name_empresa" class="col-md-4 control-label">Nome da empresa</label> <div class="form-group"> <input id="name_empresa" type="text" class="form-control" name="name_empresa" value="{{ old('name_empresa') }}" required autofocus> @if ($errors->has('name_empresa')) <span class="help-block"> <strong>{{ $errors->first('name_empresa') }}</strong> </span> @endif </div> </div> <div class="form-group{{ $errors->has('email_empresa') ? ' has-error' : '' }}"> <label for="email_empresa" class="col-md-4 control-label">E-Mail da empresa</label> <div class="form-group"> <input id="email_empresa" type="email" class="form-control" name="email_empresa" value="{{ old('email_empresa') }}" required> @if ($errors->has('email_empresa')) <span class="help-block"> <strong>{{ $errors->first('email_empresa') }}</strong> </span> @endif </div> </div> <div class="form-group{{ $errors->has('estado_empresa') ? ' has-error' : '' }}"> <label for="estado_empresa" class="col-md-4 control-label">Estado da empresa</label> <div class="form-group"> <select class="form-group form-control" name="estado_empresa"> <option >Escolha</option> <option value="Acre">Acre</option> <option value="Alagoas">Alagoas</option> <option value="Amapá">Amapá</option> <option value="Amazonas">Amazonas</option> <option value="Bahia">Bahia</option> <option value="Ceará">Ceará</option> <option value="Distrito Federal">Distrito Federal</option> <option value="Espírito Santo">Espírito Santo</option> <option value="Goiás">Goiás</option> <option value="Maranhão">Maranhão</option> <option value="Mato Grosso">Mato Grosso</option> <option value="Mato Grosso do Sul">Mato Grosso do Sul</option> <option value="Minas Gerais">Minas Gerais</option> <option value="Pará">Pará</option> <option value="Paraíba">Paraíba</option> <option value="Paraná">Paraná</option> <option value="Pernambuco">Pernambuco</option> <option value="Piauí">Piauí</option> <option value="Rio de Janeiro">Rio de Janeiro</option> <option value="Rio Grande do Norte">Rio Grande do Norte</option> <option value="Rio Grande do Sul">Rio Grande do Sul</option> <option value="Rondônia">Rondônia</option> <option value="Roraima">Roraima</option> <option value="Santa Catarina">Santa Catarina</option> <option value="São Paulo">São Paulo</option> <option value="Sergipe">Sergipe</option> <option value="Tocantins">Tocantins</option> </select> @if ($errors->has('estado_empresa')) <span class="help-block"> <strong>{{ $errors->first('estado_empresa') }}</strong> </span> @endif </div> </div> <div class="form-group{{ $errors->has('bairro_empresa') ? ' has-error' : '' }}"> <label for="bairro_empresa" class="col-md-4 control-label">Bairro da empresa</label> <div class="form-group"> <input id="cidade_empresa" type="text" class="form-control" name="bairro_empresa" value="{{ old('bairro_empresa') }}" required> @if ($errors->has('bairro_empresa')) <span class="help-block"> <strong>{{ $errors->first('bairro_empresa') }}</strong> </span> @endif </div> </div> <div class="form-group{{ $errors->has('cidade_empresa') ? ' has-error' : '' }}"> <label for="cidade_empresa" class="col-md-4 control-label">Cidade da empresa</label> <div class="form-group"> <input id="cidade_empresa" type="text" class="form-control" name="cidade_empresa" value="{{ old('cidade_empresa') }}" required> @if ($errors->has('cidade_empresa')) <span class="help-block"> <strong>{{ $errors->first('cidade_empresa') }}</strong> </span> @endif </div> </div> <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}"> <label for="cnpj" class="col-md-4 control-label">CNPJ</label> <div class="form-group"> <input id="cnpj" type="text" class="form-control" name="cnpj" value="{{ old('cnpj') }}" required> @if ($errors->has('cnpj')) <span class="help-block"> <strong>{{ $errors->first('cnpj') }}</strong> </span> @endif </div> </div> <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}"> <label for="password" class="col-md-4 control-label">Senha</label> <div class="form-group"> <input id="password" type="password" class="form-control" name="password" required> @if ($errors->has('password')) <span class="help-block"> <strong>{{ $errors->first('password') }}</strong> </span> @endif </div> </div> <div class="form-group"> <label for="password-confirm" class="col-md-4 control-label">Confirmar senha</label> <div class="form-group"> <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required> </div> </div> <div class="form-group"> <div class="form-group"> <button type="submit" class="btn btn-primary"> Registrar </button> </div> </div>