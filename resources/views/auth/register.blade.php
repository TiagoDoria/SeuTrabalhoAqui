@extends('layouts.app')

@section('content')
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<body id="page-top">
  <!-- Navigation -->
  <nav id="mainNav" class="navbar navbar-expand-lg navbar-light fixed-top menu-sec" >
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="login">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="register">Registrar</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
<div class="container">
  <br><br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <h2 class="text-center">Cadastre-se</h2>
                <br><br>
                <div class="panel-body">
                    <form class="myform" enctype="multipart/form-data" method="POST" action="{{ route('register') }}"> {{ csrf_field() }}
                  
                    <div id="forms">
                      @include('auth.include.fisica');
                     
                    </div>  
                     </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->

<footer  style="background-color:#000;position: static;bottom: 0;width: 100%;">
  <div class=" text-center">
    <p style="color:#fff;">Copyright &copy; seutrabalhoaqui 2017</p>
  </div>
</footer>

<script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <script src="/vendor/jquery/jquery.min.js"></script>
@endsection
