<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Seu Trabalho é Aqui</title>
    <!-- Bootstrap core CSS -->
   <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <!-- Custom fonts for this template -->
   <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
   <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>

   <link href="/css/sta.min.css" rel="stylesheet">
   <link href="/css/styles.css" rel="stylesheet">

   <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
   <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>
<body id="page-top">

   <!-- Navigation -->
  @if(empty(Auth()->user()->id))
      <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
          <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fa fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="/">Home</a>
              </li>

              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    @else
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="/">Home</a>
            </li>


            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>

            </li>

            <li style="color:#fff; padding: 2px;" class="nav-item">
               <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents">
              <i class="material-icons">account_circle</i>&nbsp; <?php $nome = explode(" ", Auth()->user()->name); echo $nome[0]; ?>
               </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents">
                  <li><a href="{{ route('home') }}">Minha conta</a></li>
                  <li><a href="{{ route('meu_perfil') }}">Perfil</a></li>
                  <li> <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">Sair</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form></li>
                </ul>
              </li>

          </ul>
        </div>
      </div>
    </nav>
    @endif
  <section id="contato" class="content-section text-center">
    <div class="container">
      <div class="col-md-12">
        <div class="">
          <br><br>
          <h3 class="text-center">Entre em contato</h3><br>
          <p class="text-center">Se tem alguma dúvida, sugestão ou crítica, por favor, entre em contato.</p><br>
          <form  action="{{ route('envio') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-4 text-center">
                <label style="font-weight:bolder;">Nome: *</label>
                <input type="text" class="form-control" name="nome" placeholder="Nome" required>
              </div>
              <div class="col-md-4 text-center">
                <label style="font-weight:bolder;">E-mail: *</label>
                <input type="text" class="form-control" name="email" placeholder="E-mail">
              </div>
              <div class="col-md-4 text-center">
                <label style="font-weight:bolder;">Cidade:</label>
                <input type="text" class="form-control" name="cidade" placeholder="Cidade">
              </div>
            </div><br>

            <div class="form-group col-md-12">
              <label for="comment" style="font-weight:bolder;">Mensagem *</label>
              <textarea class="form-control" rows="10" id="mensagem" name="mensagem" required></textarea>
            </div>
            <button type="submit" class="btn btn-primary text-center">Enviar</button>
          </form>
        </div>
      </div>
    </div>


  </section>




  <!-- Footer -->
  <footer style="background-color:#000;">
    <div class="container text-center">
      <p style="color:#fff;">Copyright &copy; seutrabalhoaqui 2017</p>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/popper/popper.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

  <!-- Custom scripts for this template -->
  <script src="/js/sta.min.js"></script>

</body>


</html>
