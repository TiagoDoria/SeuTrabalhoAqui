<!DOCTYPE html>
<html lang="en">

   <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Seu Trabalho é Aqui</title>
    <!-- Bootstrap core CSS -->
   <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <!-- Custom fonts for this template -->
   <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
   <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>

   <link href="/css/grayscale.min.css" rel="stylesheet">
   <link href="/css/styles.css" rel="stylesheet">

   <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   
   <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
   <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <body id="page-top">
    @if(empty(Auth()->user()->id))
      <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
          <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fa fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="/">Home</a>
              </li>
              
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    @else
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="/">Home</a>
            </li>
           
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>

            </li>

            <li style="color:#fff; padding: 2px;" class="nav-item">
               <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents">
              <i class="material-icons">account_circle</i>&nbsp; <?php $nome = explode(" ", Auth()->user()->name); echo $nome[0]; ?>
               </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents">
                  <li><a href="{{ route('home') }}">Entrar</a></li>
                  <li><a href="{{ route('meu_perfil') }}">Perfil</a></li>
                  <li> <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">Sair</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form></li>     
                </ul>
              </li>

          </ul>
        </div>
      </div>
    </nav>
    @endif
   
 <div class="container">
  <br><br><br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <h2 class="text-center">Cadastre sua oportunidade</h2>
                <br><br>
                <div class="panel-body">
                    <form class="myform" method="POST" action="{{ route('cadastro_oportunidade') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('tipo_cont') ? ' has-error' : '' }}">
                            <label for="tipo_cont" class="col-md-4 control-label">Tipo de contratação*</label>

                            <div class="form-group" id="tipo_cont">

                                <select class="form-group form-control" name="tipo_cont" required aria-required="true">
                                <option value="" >Escolha</option>
                                <option value="clt">CLT</option>
                                <option value="estagio">Estágio</option>
                                <option value="jovem_aprendiz">Jovem aprendiz</option>
                                <option value="temporario">Temporário</option>
                                                               
                                </select>

                                @if ($errors->has('tipo_cont'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tipo_cont') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('nome_empresa') ? ' has-error' : '' }}">
                            <label for="pais" class="col-md-4 control-label">Nome da contratante*</label>

                            <div class="form-group">
                                <input id="nome_empresa" type="text" class="form-control" name="nome_empresa" value="{{ old('nome_empresa') }}" required>

                                @if ($errors->has('nome_empresa'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nome_empresa') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email_empresa') ? ' has-error' : '' }}">
                            <label for="pais" class="col-md-4 control-label">E-mail para contato*</label>

                            <div class="form-group">
                                <input id="email_empresa" type="text" class="form-control" name="email_empresa" value="{{ old('email_empresa') }}" required>

                                @if ($errors->has('email_empresa'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email_empresa') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('cargo') ? ' has-error' : '' }}">
                            <label for="pais" class="col-md-4 control-label">Cargo*</label>

                            <div class="form-group">
                                <input id="cargo" type="text" class="form-control" name="cargo" value="{{ old('cargo') }}" required>

                                @if ($errors->has('cargo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cargo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="carga_horaria" class="col-md-4 control-label">Carga Horária(Semanal)*</label>

                            <div class="form-group">
                                <input id="carga_horaria" type="text" class="form-control" name="carga_horaria" value="{{ old('carga_horaria') }}" required>

                                @if ($errors->has('carga_horaria'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('carga_horaria') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="form-group{{ $errors->has('turno') ? ' has-error' : '' }}">
                            <label for="turno" class="col-md-4 control-label">Turno*</label>

                            <div class="form-group">

                                <select class="form-group form-control" name="turno" required aria-required="true">
                                <option value="" >Escolha</option>
                                <option value="matutino">Matutino</option>
                                <option value="vespertino">Vespertino</option>
                                <option value="flexivel">Flexível</option>
                                </select>

                                @if ($errors->has('turno'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('turno') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('estado') ? ' has-error' : '' }}">
                            <label for="estado" class="col-md-4 control-label">Estado*</label>

                            <div class="form-group">

                                <select class="form-group form-control" name="estado" required aria-required="true">
                                <option value="" >Escolha</option>
                                <option value="Acre">Acre</option>
                                <option value="Alagoas">Alagoas</option>
                                <option value="Amapá">Amapá</option>
                                <option value="Amazonas">Amazonas</option>
                                <option value="Bahia">Bahia</option>
                                <option value="Ceará">Ceará</option>
                                <option value="Distrito Federal">Distrito Federal</option>
                                <option value="Espírito Santo">Espírito Santo</option>
                                <option value="Goiás">Goiás</option>
                                <option value="Maranhão">Maranhão</option>
                               <option value="Mato Grosso">Mato Grosso</option>
                                <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                                <option value="Minas Gerais">Minas Gerais</option>
                                <option value="Pará">Pará</option>
                                <option value="Paraíba">Paraíba</option>
                                <option value="Paraná">Paraná</option>
                                <option value="Pernambuco">Pernambuco</option>
                                <option value="Piauí">Piauí</option>
                                <option value="Rio de Janeiro">Rio de Janeiro</option>
                                <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                                <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                                <option value="Rondônia">Rondônia</option>
                                <option value="Roraima">Roraima</option>
                                <option value="Santa Catarina">Santa Catarina</option>
                                <option value="São Paulo">São Paulo</option>
                                <option value="Sergipe">Sergipe</option>
                                <option value="Tocantins">Tocantins</option>
                                </select>

                                @if ($errors->has('estado'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('estado') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('cidade') ? ' has-error' : '' }}">
                            <label for="cidade" class="col-md-4 control-label">Cidade*</label>

                            <div class="form-group">
                                <input id="cidade" type="text" class="form-control" name="cidade" value="{{ old('cidade') }}" required>

                                @if ($errors->has('cidade'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cidade') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('bairro') ? ' has-error' : '' }}">
                            <label for="bairro" class="col-md-4 control-label">Bairro*</label>

                            <div class="form-group">
                                <input id="bairro" type="text" class="form-control" name="bairro" value="{{ old('bairro') }}" required>

                                @if ($errors->has('bairro'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bairro') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('salario') ? ' has-error' : '' }}">
                            <label for="salario" class="col-md-4 control-label">Salário(Não orbrigatório informar)</label>

                            <div class="form-group">
                                <input id="salario" type="number" step='any' class="form-control" name="salario" value="{{ old('salario') }}">

                                @if ($errors->has('salario'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('salario') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div id="estagios">
                          
                        </div>



                        <div class="form-group{{ $errors->has('beneficios') ? ' has-error' : '' }}">
                            <label for="beneficios" class="col-md-4 control-label">Benefícios*</label>

                            <div class="form-group">

                              <textarea class="form-control" rows="5" id="beneficios" name="beneficios" ></textarea>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pre_requisitos') ? ' has-error' : '' }}">
                            <label for="descricao" class="col-md-4 control-label">Pré-requisitos</label>

                            <div class="form-group">

                              <textarea class="form-control" rows="5" id="pre_requisitos" name="pre_requisitos" ></textarea>
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                            <label for="descricao" class="col-md-4 control-label">Descrição da oportunidade*</label>

                            <div class="form-group">

                              <textarea class="form-control" rows="5" id="descricao" name="descricao" required></textarea>
                            </div>
                        </div>




                        <div class="form-group">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fo
    <!-- Footer -->
    <footer style="background-color:#000;">
      <div class="container text-center">
        <p style="color:#fff;">Copyright &copy; seutrabalhoaqui 2017</p>
      </div>
    </footer>

    <script>
    $('#tipo_cont').on('change', function (y){
            var tipo = y.target.value;
            $('#estagios').empty();
            
            if(tipo == 'estagio')
                $('#estagios').append('@include('layouts.estagio')');
            if(tipo == 'temporario')
                $('#estagios').append('@include('trabalho.include.temporario')');  
            });
    </script>

   <!-- Bootstrap core JavaScript -->
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="/vendor/chart.js/Chart.min.js"></script>
    <script src="/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for this template -->
    <script src="/js/sta-admin.min.js"></script>
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/popper/popper.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

    <!-- Custom scripts for this template -->
    <script src="/js/sta.min.js"></script>

  </body>


</html>
