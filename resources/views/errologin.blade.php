<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Seu Trabalho é Aqui</title>
    <!-- Bootstrap core CSS -->
   <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <!-- Custom fonts for this template -->
   <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
   <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>

   <link href="/css/grayscale.min.css" rel="stylesheet">
   <link href="/css/styles.css" rel="stylesheet">

   <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  </head>
<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <section id="contato" class="content-section text-center">
    <div class="container">
      <div class="col-md-12">
        <div class="">
          <br><br>
          <h3 class="text-center">Erro!</h3><br>
          <p class="text-center">É necessário realizar login para acessar essa página.</p><br>
          <a href="{{route('login')}}"><p class="text-center">Clique aqui para realizar login.</p></a><br>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer style="background-color:#000;">
    <div class="container text-center">
      <p style="color:#fff;">Copyright &copy; seutrabalhoaqui 2017</p>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/popper/popper.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

  <!-- Custom scripts for this template -->
  <script src="/js/sta.min.js"></script>

</body>


</html>
