<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Seu Trabalho é Aqui</title>

    <!-- Bootstrap core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/sta-admin.css" rel="stylesheet">

  </head>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">

    @if(Session::has('flash_message_error'))
    <div class="text-center alert alert-danger"><span  class="text-center glyphicon glyphicon-ok"></span><em> {!! session('flash_message_error') !!}</em></div>
    @endif

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <a class="navbar-brand" href="/">SeuTrabalhoAqui</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
          <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a class="nav-link" href="/home">
              <i class="fa fa-fw fa-dashboard"></i>
              <span class="nav-link-text">
                Página inicial</span>
            </a>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text">
                Meu currículo </span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseComponents">
              <li>
                <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">1)  Cadastrar currículo</a>
                <ul class="sidenav-third-level collapse" id="collapseMulti2">">
                  
                    <li>
                      <a href="{{ route('dados_pessoais_emp')}}">1.1) Dados pessoais</a>
                    </li>
                    <li>
                      <a href="{{ route('experiencia_emprego')}}">1.2) Adicionar experiência</a>
                    </li>
                    <li>
                      <a href="{{ route('formacao_emprego')}}">1.3) Adicionar formação</a>
                    </li>

                  
                </ul>
              </li>
              <li>
                <a href="{{ route('editar_curriculo')}}">2) Atualizar currículo</a>
              </li>
              <li>
                <a href="{{ route('meucurriculo') }}">3) Visualizar meu currículo</a>
              </li>

            </ul>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#minhasvagas" data-parent="#exampleAccordion">
              <i class="fa fa-fw fa-sitemap"></i>
              <span class="nav-link-text">
                Minhas vagas</span>
            </a>
            <ul class="sidenav-second-level collapse" id="minhasvagas">
              <li>
                <a href="{{ route('vagas_ofertadas')}}">Ofertadas</a>
              </li>
              <li>
                <a href="#">Concorrendo</a>
              </li>
            </ul>
          </li>

        </ul>
        <ul class="navbar-nav sidenav-toggler">
          <li class="nav-item">
            <a class="nav-link text-center" id="sidenavToggler">
              <i class="fa fa-fw fa-angle-left"></i>
            </a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <li style="color:#fff;padding:6px;" class="nav-item">Bem vindo,{{ Auth::user()->name }} ( <i class="fa fa-user-circle" aria-hidden="true"></i> <a href="{{ route('meu_perfil')}}"> Ver perfil </a>)</li>
          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">

              <i class="fa fa-fw fa-sign-out"></i>
              Sair</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
          </li>
        </ul>
      </div>
    </nav>

    <div class="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Página principal</li>
        </ol>

        <div class="row">

          <div class="col-lg-8">

            <!-- Example Bar Chart Card -->
            <div class="card mb-3">
              <div class="card-header">
                <i class="fa fa-bar-chart"></i>
                Estatísticas
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-8 my-auto">
                    <canvas id="myBarChart" width="100" height="50"></canvas>
                  </div>
                  <div class="col-sm-4 text-center my-auto">
                    <div class="h4 mb-0 text-primary ">34693</div>
                    <div class="small text-muted">Usuários cadastrados</div>
                    <hr>
                    <div class="h4 mb-0 text-warning">18474</div>
                    <div class="small text-muted">Vagas cadastradas</div>
                    <hr>
                    <div class="h4 mb-0 text-success">16219</div>
                    <div class="small text-muted">Usuários beneficiados</div>
                  </div>
                </div>
              </div>

            </div>


          </div>

      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- /.content-wrapper -->

    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright &copy; seutrabalhoaqui 2017</small>
        </div>
      </div>
    </footer>

    <!-- Scroll to Top Button -->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>


    <!-- Bootstrap core JavaScript -->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/popper/popper.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="/vendor/chart.js/Chart.min.js"></script>
    <script src="/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/sta-admin.min.js"></script>

  </body>

</html>
