<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Seu Trabalho é Aqui</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="css/landing-page.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>
    <link href="css/home.css" rel="stylesheet">
    <link href="css/sta.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>

  <body id="page-top">
    @if(empty(Auth()->user()->id))
      <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
          <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fa fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="/">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#about">Sobre</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#buscar">Buscar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#divulgar">Divulgar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    @else
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="/">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Sobre</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#buscar">Buscar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#divulgar">Divulgar</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>

            </li>

            <li style="color:#fff; padding: 2px;" class="nav-item">
               <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents">
              <i class="material-icons">account_circle</i>&nbsp; <?php $nome = explode(" ", Auth()->user()->name); echo $nome[0]; ?>
               </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents">
                  <li><a href="{{ route('home') }}">Minha conta</a></li>
                  <li><a href="{{ route('meu_perfil') }}">Perfil</a></li>
                  <li> <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">Sair</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form></li>     
                </ul>
              </li>

          </ul>
        </div>
      </div>
    </nav>
    @endif
    <header class="masthead">
      <div class="intro-body">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h1 class="">Encontre o seu trabalho aqui!</h1>
              <p class="intro-text"><strong>Há {{$cont}} oportunidades de trabalho esperando por você.</strong></p>
              @if(Session::has('flash_message_error'))
              <div class="text-center alert alert-danger"><span  class="text-center glyphicon glyphicon-ok"></span><em> {!! session('flash_message_error') !!}</em></div>
              @endif
              <form method="post" action="search_index">
                  {{ csrf_field() }}

                <div class="col-md-10" id="input-mobile">
                  <input type="text" class="form-control" id="searchjob" name="cargo_index" placeholder="Ex.: Analista de sistemas" required>
                </div>
                  <div class="col-md-10 form-group{{ $errors->has('estado') ? ' has-error' : '' }}" id="select-mobile">

                      <div class="form-group col-md-10" id="select-mobile">

                          <select required aria-required="true" class="form-group form-control" name="estado_index" >
                          <option value="">Estado</option>
                          <option value="Acre">Acre</option>
                          <option value="Alagoas">Alagoas</option>
                          <option value="Amapá">Amapá</option>
                          <option value="Amazonas">Amazonas</option>
                          <option value="Bahia">Bahia</option>
                          <option value="Ceará">Ceará</option>
                          <option value="Distrito Federal">Distrito Federal</option>
                          <option value="Espírito Santo">Espírito Santo</option>
                          <option value="Goiás">Goiás</option>
                          <option value="Maranhão">Maranhão</option>
                          <option value="Mato Grosso">Mato Grosso</option>
                          <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                          <option value="Minas Gerais">Minas Gerais</option>
                          <option value="Pará">Pará</option>
                          <option value="Paraíba">Paraíba</option>
                          <option value="Paraná">Paraná</option>
                          <option value="Pernambuco">Pernambuco</option>
                          <option value="Piauí">Piauí</option>
                          <option value="Rio de Janeiro">Rio de Janeiro</option>
                          <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                          <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                          <option value="Rondônia">Rondônia</option>
                          <option value="Roraima">Roraima</option>
                          <option value="Santa Catarina">Santa Catarina</option>
                          <option value="São Paulo">São Paulo</option>
                          <option value="Sergipe">Sergipe</option>
                          <option value="Tocantins">Tocantins</option>
                          </select>

                          @if ($errors->has('estado'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('estado') }}</strong>
                              </span>
                          @endif
                      </div>
                      
                  </div>
                  <div class="col-md-4" id="send-mobile">
                    <input type="submit" class="form-control" value="Buscar">
                  </div>
                  
               
              
              </form>
              <br><br>

            </div>
          </div>
        </div>
      </div>
    </header>

    <section id="about" class="content-section text-center">
      <div class="container">

        <div class="row">

          <div class="col-lg-5 ml-auto">
            <hr class="section-heading-spacer">
            <div class="clearfix"></div>
            <h1 class="section-heading">Sobre</h1>
            <p class="lead">O 'Seu Trabalho Aqui' é uma plataforma que tem como objetivo contribuir com a redução do índice de desemprego no país, facilitando a comunicação entre o contratante e o trabalhador e fornecendo um meio eficiente e simples de divulgação.</p>
          </div>
          <div class="col-lg-5 mr-auto">
            <img class="img-fluid" src="img/negocios.jpg" alt="" style="border-radius:15px;">
          </div>
        </div>

      </div>
      <br><br>
      <div class="container">

        <div class="row">
          <div class="col-lg-5 mr-auto order-lg-2">
            <hr class="section-heading-spacer">
            <div class="clearfix"></div>
            <h2 class="section-heading">O seu futuro é aqui</h2>
            <p class="lead">Para o candidato, será possível se candidatar pela própria plataforma, gerar seu currículo, além de permitir o download. O sistema irá mostrar estatísticas de outros candidatos concorrentes a uma vaga, além de gerar feedback sobre o processo seletivo do contratante(opcional).</p>
          </div>
          <br><br>
          <div class="col-lg-5 ml-auto order-lg-1">
            <img class="img-fluid" src="img/job.jpg" alt="">
          </div>

        </div>
         <p class="lead">Para o contratante, será possível acessar todos os candidatos a uma vaga específica divulgada, filtrando os candidatos que melhor satisfaçam aos pré-requisitos exigidos para a vaga. Além disso, o contratante terá opção de mandar um feedback para o candidato caso ele tenha condições de prosseguir ou não no processo seletivo.</p>
         <p>Por fim, será possível agendar entrevistas, indicando local. data e hora e também terá uma área reservada para dicas ou sugestões para ajudar ao próximo a conquistar o seu tão sonhado emprego.</p>
      </div>

    </section>

    <section id="buscar" class="text-center">
       <br><br>
       <div class="col-lg-8 mx-auto"> <h2>Encontre oportunidades!</h2> <p>Procure pela oportunidade desejada aqui.</p> </div>
       <br><br>
        @include('layouts.buscar')
        @include('layouts.buscar2')
    </section>

    <section id="divulgar" class="content-section text-center">
      <div class="container">
        <div class="col-lg-8 mx-auto">
          <h2>Divulgar uma oportunidade</h2>
          <p>Aqui você pode divulgar uma oportunidade de trabalho da sua preferência.</p>
          <a href="divulgar_emprego" class="btn btn-default btn-lg">Clique aqui</a>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer style="background-color:#000;">
      <div class="container text-center">
        <p style="color:#fff;">Copyright &copy; seutrabalhoaqui 2018</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

    <!-- Custom scripts for this template -->
    <script src="js/sta.min.js"></script>

  </body>


</html>
