<h3>Parabéns! Mais um interessado na sua vaga.</h3>
<h3>Nome: </h3> {{ $nome }}
<h4>Mensagem: </h4>  {{ $mensagem }}

<h3>Detalhes da vaga</h3>
<h4>Cargo: </h4> {{$vaga}}
<h4>Descrição: </h4> {{$descricao}}
<h3>Clique para efetuar o download do currículo do candidato: </h3> <a href="{{ route('download_curriculo',['id'=>encrypt($id)])}}">Download</a>
