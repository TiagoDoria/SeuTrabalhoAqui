<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="">
  <meta name="author" content="">
  <title>Seu Trabalho é Aqui</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- Plugin CSS -->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sta-admin.css" rel="stylesheet">

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="/">SeuTrabalhoAqui</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link text-center" href="/home">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">
              Página inicial</span>
            </a>
          </li>

          <li class="nav-item text-center" data-toggle="tooltip"  title="Components">
           
              <span style="color:#fff;font-weight: bolder;" class="nav-link-text">
                Meu currículo </span><hr>

           </li>  
           <li class="nav-item text-center" data-toggle="tooltip"  title="Components">
              <a style="color:#fff;" class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">
              <span style="color:#fff;float: center;" class="nav-link-text">
                1) Cadastrar currículo </span></a>
                <div class="collapse" id="collapseMulti2">">
                    
                      <p>
                        <a style="color:#fff;" href="{{ route('dados_pessoais_emp')}}">1.1) Dados pessoais</a>
                      </p>
                      <p class="text-center">
                        <a style="color:#fff;" href="{{ route('experiencia_emprego')}}">1.2) Adicionar experiência</a>
                      </p>
                      <p class="text-center">
                        <a  style="color:#fff;" href="{{ route('formacao_emprego')}}">1.3) Adicionar formação</a><br>
                      </p>

                    <hr>
                      </div>

           </li>  
           <li class="nav-item text-center" data-toggle="tooltip"  title="Components">
           
              <a href="{{ route('editar_curriculo')}}"><span style="color:#fff;float: center;" class="nav-link-text">
                2) Editar currículo </span></a>

           </li>  

           <li class="nav-item text-center" data-toggle="tooltip"  title="Components">
            
              <a href ="{{ route('meucurriculo') }}"><span style="color:#fff;float: center;" class="nav-link-text">
                3) Visualizar currículo </span></a>

           </li>  
            
           <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
              <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#minhasvagas" data-parent="#exampleAccordion">
              <i class="fa fa-fw fa-sitemap"></i>
              <span class="nav-link-text">
                    Minhas vagas</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="minhasvagas">
                     
                      <li>
                        <a href="{{ route('vagas_ofertadas')}}">Ofertadas</a>
                      </li>
                      <li>
                        <a href="#">Concorrendo</a>
                      </li>
                    </ul>
                  </li>

                </ul>
                <ul class="navbar-nav sidenav-toggler">
                  <li class="nav-item">
                    <a class="nav-link text-center" id="sidenavToggler">
                      <i class="fa fa-fw fa-angle-left"></i>
                    </a>
                  </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                  <li style="color:#fff;padding:6px;" class="nav-item">Bem vindo,{{ Auth::user()->name }} ( <i class="fa fa-user-circle" aria-hidden="true"></i> <a href="{{ route('meu_perfil')}}"> Ver perfil </a>)</li>
                  <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">

                    <i class="fa fa-fw fa-sign-out"></i>
                    Sair</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </li>
                </ul>
              </div>
            </nav>
            <div class="content-wrapper">

              <div class="container-fluid">

                <!-- Breadcrumbs -->
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item active">Página principal</li>
                </ol>

                <div class="">
                  <form action="{{ route('addexpr') }}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group">
                      <h2>Experiência profissional</h2>

                      <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label">Empresa</label>
                        <input type="text" class="form-control" id="empresa" name="empresa" placeholder="Empresa" required>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label">Período</label>
                        <input type="text" class="form-control" id="periodo" name="periodo" placeholder="Período" required>
                      </div>
                      <div class="form-group">
                        <label for="comment">Descrição das atividades</label>
                        <textarea class="form-control" rows="5" id="desc_atv" name="desc_atv" required></textarea>
                      </div>

                    </div>

                    <button type="submit" class="btn btn-primary text-center">Cadastrar</button>
                  </form>

                </div>
                <!-- /.container-fluid -->

              </div>
              <!-- /.content-wrapper -->
              <br>
              <footer class="sticky-footer">
                <div class="container">
                  <div class="text-center">
                    <small>Copyright &copy; seutrabalhoaqui 2017</small>
                  </div>
                </div>
              </footer>

              <!-- Scroll to Top Button -->
              <a class="scroll-to-top rounded" href="#page-top">
                <i class="fa fa-angle-up"></i>
              </a>


              <!-- Bootstrap core JavaScript -->
              <script src="vendor/jquery/jquery.min.js"></script>
              <script src="vendor/popper/popper.min.js"></script>
              <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

              <!-- Plugin JavaScript -->
              <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
              <script src="vendor/chart.js/Chart.min.js"></script>
              <script src="vendor/datatables/jquery.dataTables.js"></script>
              <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

              <!-- Custom scripts for this template -->
              <script src="js/sta-admin.min.js"></script>

            </body>

            </html>
