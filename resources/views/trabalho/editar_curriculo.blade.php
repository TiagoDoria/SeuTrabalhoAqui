<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="">
  <meta name="author" content="">
  <title>Seu Trabalho é Aqui</title>

  <!-- Bootstrap core CSS -->
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- Plugin CSS -->
  <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="/css/sta-admin.css" rel="stylesheet">

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="/">SeuTrabalhoAqui</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item active" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link text-center" href="/home">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">
              Página inicial</span>
            </a>
          </li>

          <li class="nav-item text-center" data-toggle="tooltip"  title="Components">
           
              <span style="color:#fff;font-weight: bolder;" class="nav-link-text">
                Meu currículo </span><hr>

           </li>  
           <li class="nav-item text-center" data-toggle="tooltip"  title="Components">
              <a style="color:#fff;" class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">
              <span style="color:#fff;float: center;" class="nav-link-text">
                1) Cadastrar currículo </span></a>
                <div class="collapse" id="collapseMulti2">">
                    
                      <p>
                        <a style="color:#fff;" href="{{ route('dados_pessoais_emp')}}">1.1) Dados pessoais</a>
                      </p>
                      <p class="text-center">
                        <a style="color:#fff;" href="{{ route('experiencia_emprego')}}">1.2) Adicionar experiência</a>
                      </p>
                      <p class="text-center">
                        <a  style="color:#fff;" href="{{ route('formacao_emprego')}}">1.3) Adicionar formação</a><br>
                      </p>

                    <hr>
                      </div>

           </li>  
           <li class="nav-item text-center" data-toggle="tooltip"  title="Components">
           
              <a href="{{ route('editar_curriculo')}}"><span style="color:#fff;float: center;" class="nav-link-text">
                2) Editar currículo </span></a>

           </li>  

           <li class="nav-item text-center" data-toggle="tooltip"  title="Components">
            
              <a href ="{{ route('meucurriculo') }}"><span style="color:#fff;float: center;" class="nav-link-text">
                3) Visualizar currículo </span></a>

           </li>  
            
           <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
              <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#minhasvagas" data-parent="#exampleAccordion">
              <i class="fa fa-fw fa-sitemap"></i>
              <span class="nav-link-text">
                    Minhas vagas</span>
                    </a>
                    <ul class="sidenav-second-level collapse" id="minhasvagas">
                     
                      <li>
                        <a href="{{ route('vagas_ofertadas')}}">Ofertadas</a>
                      </li>
                      <li>
                        <a href="#">Concorrendo</a>
                      </li>
                    </ul>
                  </li>

                </ul>
                <ul class="navbar-nav sidenav-toggler">
                  <li class="nav-item">
                    <a class="nav-link text-center" id="sidenavToggler">
                      <i class="fa fa-fw fa-angle-left"></i>
                    </a>
                  </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                  <li style="color:#fff;padding:6px;" class="nav-item">Bem vindo,{{ Auth::user()->name }} ( <i class="fa fa-user-circle" aria-hidden="true"></i> <a href="{{ route('meu_perfil')}}"> Ver perfil </a>)</li>
                  <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">

                    <i class="fa fa-fw fa-sign-out"></i>
                    Sair</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </li>
                </ul>
              </div>
            </nav>
            <div class="content-wrapper">

              <div class="container-fluid">

                <!-- Breadcrumbs -->
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item active">Página principal</li>
                </ol>

                <div class="">
                  <form action="{{ route('editar_c', encrypt(Auth::user()->id)) }}" method="post">
                    {{ csrf_field() }}
                    @foreach ($pessoais as $pes)
                    <h2>Dados pessoais</h2>
                    <div class="form-row">

                      <div class="form-group col-md-6">
                        <label for="inputEmail4" class="col-form-label">Naturalidade</label>
                        <input type="text" class="form-control" name="naturalidade" id="naturalidade" value="{{$pes->naturalidade}}" >
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputPassword4" class="col-form-label">Idade</label>
                        <input type="number" class="form-control" name="idade" id="idade" value="{{$pes->idade}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputAddress" class="col-form-label">Endereço</label>
                      <input type="text" class="form-control" id="endereco" name="endereco" value="{{$pes->endereco}}">
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label">Celular</label>
                        <input type="text" class="form-control" id="celular" name="celular" value="{{$pes->celular}}">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label">Objetivo</label>
                        <input type="text" class="form-control" id="celular" name="objetivo" value="{{$pes->objetivo}}">
                      </div>
                    </div>
                    

                    <div class="form-group">
                      <h2>Qualificações</h2>

                      <div class="form-group">
                        <label for="comment">Descrição</label>
                        <textarea class="form-control" rows="5" id="desc_qua" name="desc_qua">{{$pes->qualificacoes}}</textarea>
                      </div>

                    </div>
                    <div class="form-group">
                      <h2>Informações adicionais</h2>

                      <div class="form-group">
                        <label for="comment">Descrição</label>
                        <textarea class="form-control" rows="5" id="info_add" name="info_add">{{$pes->info_add}}</textarea>
                      </div>


                    </div>
                    @endforeach
                    @foreach ($expr as $ex)
                    <div class="form-grou p">
                      <h2>Experiência profissional</h2>

                      <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label">Empresa</label>
                        <input type="text" class="form-control" id="empresa" name="empresa[]" value="{{$ex->empresa}}">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label">Período</label>
                        <input type="text" class="form-control" id="periodo" name="periodo[]" value="{{$ex->periodo}}">
                      </div>
                      <div class="form-group">
                        <label for="comment">Descrição das atividades</label>
                        <textarea class="form-control" rows="5" id="desc_atv" name="desc_atv[]" required>{{$ex->desc_atv}}</textarea>
                      </div>
                      <div class="form-group col-md-6">
                        <input type="text" class="form-control" id="id_expr" name="id_expr[]" value="{{$ex->id}}" hidden>
                        <input type="text" class="form-control" id="id_expe" name="id_expe" value="{{$ex->id}}" hidden>

                        <a href="{{ route('excluir_expr',['id2'=>$ex->id]) }}" onclick="javascript:if(!confirm('Deseja excluir?'))return false;"  <p>x Excluir experiência x</p></a>
                      </div>

                    </div>
                    @endforeach
                    @foreach ($formacao as $form)
                    <div class="form-group">
                      <h2>Formação Acadêmica</h2>

                      <label for="inputAddress2" class="col-form-label">Escolaridade</label>
                    <div class="form-group">
                      <select class="form-group form-control" name="escolaridade">
                        <option >Escolha</option>
                        <option <?php if($form->escolaridade=='SF') echo "selected"; ?> value="SF" >Sem formação</option>
                        <option <?php if($form->escolaridade=='EFI') echo "selected"; ?> value="EFI">Ensino Fundamental incompleto</option>
                        <option <?php if($form->escolaridade=='EFC') echo "selected"; ?> value="EFC">Ensino Fundamental completo</option>
                        <option <?php if($form->escolaridade=='EMI') echo "selected"; ?> value="EMI">Ensino Médio incompleto</option>
                        <option <?php if($form->escolaridade=='EMC') echo "selected"; ?> value="EMC">Ensino Médio completo</option>
                        <option <?php if($form->escolaridade=='ESI') echo "selected"; ?> value="ESI">Ensino superior incompleto</option>
                        <option <?php if($form->escolaridade=='ESC') echo "selected"; ?> value="ESC">Ensino superior completo</option>
                      </select>

                    </div>

                      <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label">Instituição</label>
                        <input type="text" class="form-control" id="instituicao" name="instituicao[]" value="{{$form->instituicao}}">
                      </div>

                      <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label">Curso(Para nível superior)</label>
                        <input type="text" class="form-control" id="curso" name="curso[]" value="{{$form->curso}}">
                      </div>

                      <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label">Semestre(Para nível superior)</label>
                        <input type="text" class="form-control" id="semestre" name="semestre[]" value="{{$form->semestre}}">
                      </div>

                      <div class="form-group col-md-6">
                        <label for="inputAddress2" class="col-form-label">Conclusão ou Previsão de conclusão</label>
                        <input type="text" class="form-control" id="previsao" name="previsao[]" value="{{$form->ano_conclusao_previsao}}">
                      </div>
                      <div class="form-group col-md-6">
                        <input type="text" class="form-control" id="id_form" name="id_form[]" value="{{$form->id}}" hidden>
                        <input type="text" class="form-control" id="id_forme" name="id_forme" value="{{$form->id}}" hidden>
                        <input type="text" class="form-control" id="id_usuario" name="id_usuario" value="{{Auth::user()->id}}" hidden>
                        <a href="{{ route('excluir_form',['id' =>Auth::user()->id,'id2'=>$form->id]) }}" onclick="javascript:if(!confirm('Deseja excluir?'))return false;"  <p>x Excluir formação x</p></a>
                      </div>

                    </div>
                    @endforeach
                    <button type="submit" class="btn btn-primary text-center">Atualizar</button>
                  </form>

                </div>
                <!-- /.container-fluid -->

              </div>
              <!-- /.content-wrapper -->
              <br>
              <footer class="sticky-footer">
                <div class="container">
                  <div class="text-center">
                    <small>Copyright &copy; seutrabalhoaqui 2017</small>
                  </div>
                </div>
              </footer>

              <!-- Scroll to Top Button -->
              <a class="scroll-to-top rounded" href="#page-top">
                <i class="fa fa-angle-up"></i>
              </a>


              <!-- Bootstrap core JavaScript -->
              <script src="/vendor/jquery/jquery.min.js"></script>
              <script src="/vendor/popper/popper.min.js"></script>
              <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

              <!-- Plugin JavaScript -->
              <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
              <script src="/vendor/chart.js/Chart.min.js"></script>
              <script src="/vendor/datatables/jquery.dataTables.js"></script>
              <script src="/vendor/datatables/dataTables.bootstrap4.js"></script>

              <!-- Custom scripts for this template -->
              <script src="js/sta-admin.min.js"></script>

            </body>

            </html>
