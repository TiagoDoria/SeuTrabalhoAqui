<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Seu Trabalho é Aqui</title>
  <!-- Bootstrap core CSS -->
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>

  <link href="/css/small-business.css" rel="stylesheet">
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>

<body id="page-top">

 @if(empty(Auth()->user()->id))
      <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
          <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fa fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="/">Home</a>
              </li>
                  
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    @else
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

          <i class="fa fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="/">Home</a>
            </li>
          
           
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>

            </li>

            <li style="color:#fff; padding: 2px;" class="nav-item">
               <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents">
              <i class="material-icons">account_circle</i>&nbsp; <?php $nome = explode(" ", Auth()->user()->name); echo $nome[0]; ?>
               </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents">
                  <li><a href="{{ route('home') }}">Entrar</a></li>
                  <li><a href="{{ route('meu_perfil') }}">Perfil</a></li>
                  <li> <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">Sair</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form></li>     
                </ul>
              </li>

          </ul>
        </div>
      </div>

    </nav>

    @endif
    <br><br><br><br>
    <h2 style="text-align: center;">Pesquisar oportunidades</h2>
   
  @if(Session::has('flash_message_error'))
  <div class="text-center alert alert-danger"><span  class="text-center glyphicon glyphicon-ok"></span><em> {!! session('flash_message_error') !!}</em></div>
  @endif
  <section id="lista" class="content-section text-center">
    <div class="container">
      
      <div class="">
        <a class="btn " data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
          Pesquisa avançada
        </a><br>
        <a href="{{route('listar_emp')}}" ><span>Limpar filtros</span></a>

        <div class="collapse" id="collapseExample">
  <div class="card card-body">
    <section >
    <div class="col-md-12">
    
    <form class="form-inline" method="post" action="pesquisar_emp">
      {{ csrf_field() }}
      <div class="col-md-5">
      <label>Área de atuação:</label>
      <div class="input-group" style="padding:10px;">

        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
        @if(empty($area_atuacao))
        <input type="text" class="form-control"  name="area_atuacao" placeholder="Área de atuação" aria-label="Área de atuação" aria-describedby="basic-addon1">
        @else
          <input type="text" class="form-control"  name="area_atuacao" value="{{$area_atuacao}}" aria-label="Área de atuação" aria-describedby="basic-addon1">
        @endif
      </div>
      <label>Bairro:</label>
      <div class="input-group" style="padding:10px;">
        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
        @if(empty($bairro))
        <input type="text" class="form-control"  name="bairro" placeholder="Bairro" aria-label="Estado" aria-describedby="basic-addon1">
        @else
           <input type="text" class="form-control"  name="bairro" value="{{$bairro}}" aria-label="Estado" aria-describedby="basic-addon1">
        @endif
      </div>
     </div>
      <div class="col-md-5">
      <label>Cidade:</label>
      <div class="input-group" style="padding:10px;">

        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
        @if(empty($cidade))
        <input type="text" class="form-control"  name="cidade" placeholder="Cidade" aria-label="cidade" aria-describedby="basic-addon1">
        @else
          <input type="text" class="form-control"  name="cidade" value="{{$cidade}}" aria-label="cidade" aria-describedby="basic-addon1">
        @endif
      </div>
      <label>Estado:</label>
      <div class="input-group" style="padding:10px;">
        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
        @if(empty($estado))
        <input type="text" class="form-control"  name="estado" placeholder="Estado" aria-label="Estado" aria-describedby="basic-addon1">
        @else
          <input type="text" class="form-control"  name="estado" value="{{$estado}}" aria-label="Estado" aria-describedby="basic-addon1">
        @endif
      </div>

     </div>
        <div  style="padding:10px;text-align: center;">

          <input  style="cursor:pointer;"  type="submit" class="form-control" value="Pesquisar">
        </div>

      </form>
    </section>
  </div>
</div>
        
         
        <div class="card card-outline-secondary my-4">
          <div class="card-header">
            {{$count}} oportunidades encontradas em geral!
          </div>

          <div class="card-body">
           
            @foreach ($trabalho as $trab)

            <p><i class="fa fa-map-marker" aria-hidden="true"></i><span style="font-weight:bolder;"> Local:</span> {{$trab->bairro}} - {{$trab->cidade}} - {{$trab->estado}}.<br> <i class="fa fa-suitcase" aria-hidden="true"></i><span style="font-weight:bolder;"> Cargo:</span> {{$trab->cargo}}.
            <i class="fa fa-hourglass-half" aria-hidden="true"></i><span style="font-weight:bolder;"> Carga horária:</span> {{$trab->carga_horaria}}h. <i class="fa fa-clock-o" aria-hidden="true"></i><span style="font-weight:bolder;"> Turno:</span> Matutino.</p>

            <a href="{{ route('ver_vaga',$trab->id) }}" class="btn">Clique para visualizar</a> <br>
            <small class="text-muted">Postado por {{$trab->nome_contratante}} em {{$trab->created_at}}</small>

            <br>
            <hr>
            </a>

            @endforeach
          </div>
        </div>
        <div >
          <h3  class="text-center">{!! $trabalho->links();!!}</h3>
        </div>

      </div>



    </div>


  </section>

</div>

  <!-- Footer -->
  <footer style="background-color:#000;">
    <div class="container text-center">
      <p style="color:#fff;">Copyright &copy; seutrabalhoaqui 2017</p>
    </div>
  </footer>


  <!-- Bootstrap core JavaScript -->
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/popper/popper.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

  <!-- Custom scripts for this template -->
  <script src="/js/sta.min.js"></script>
  <script src="/js/bootstrap.js"></script>
</body>


</html>
