<head>
@if($curriculop)
<title>CV{{$curriculo->name}}</title>
<link href="css/curriculo.css" rel="stylesheet">
</head>

  <p class="upper">{{$curriculo->name}}</p>

  <p>{{$curriculop->naturalidade}},solteiro,{{$curriculop->idade}} anos<br>{{$curriculop->endereco}}<br>
    {{$curriculo->bairro}},{{$curriculo->cidade}},{{$curriculo->estado}}<br>Telefone: {{$curriculop->celular}} / E-mail: {{$curriculo->email}}</p>

  <p>OBJETIVO<br>_____________________________________________________________________<br><br>{{$curriculop->objetivo}}</p>


  <p>FORMAÇÃO<br>_____________________________________________________________________<br><br>

  @foreach ($formacao as $form)
    - @if($form->curso){{$form->curso}},@endif @if($form->semestre){{$form->semestre}}º semestre @endif {{$form->instituicao}}, Conclusão/Previsão em {{$form->ano_conclusao_previsao}}<br>
  @endforeach
  </p>

  @if(count($expr)>0)
    <p>EXPERIÊNCIAS PROFISSIONAIS<br>_____________________________________________________________________<br><br>

      @foreach ($expr as $ex)
        - <span style="font-weight:bolder">Empresa: </span> {{$ex->empresa}}<br>
        -  <span style="font-weight:bolder">Período: </span>{{$ex->periodo}}<br>
        -  <span style="font-weight:bolder">Principais atividades: </span>{{$ex->desc_atv}}<br><br>

      @endforeach
      </p>
  @endif

  @if($curriculop->qualificacoes)
  <p>QUALIFICAÇÕES<br>_____________________________________________________________________<br><br>
    {{@$curriculop->qualificacoes}}
  </p>
  @endif

  @if($curriculop->qualificacoes)
  <p>INFORMAÇÕES ADICIONAIS<br>_____________________________________________________________________<br><br>
    {{@$curriculop->info_add}}
  </p>
  @endif
@endif
