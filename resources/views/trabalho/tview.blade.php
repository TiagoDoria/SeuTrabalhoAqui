<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Seu Trabalho é Aqui</title>
  <!-- Bootstrap core CSS -->
  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>
  <link href="css/blog-home.css" rel="stylesheet">
 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="/css/styles.css" rel="stylesheet">

  <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

</head>

<body id="page-top">
  <!-- Navigation -->
  @if(empty(Auth()->user()->id))
      <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
          <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fa fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="/">Home</a>
              </li>
                  
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    @else
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="/">SeuTrabalhoAqui</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="/">Home</a>
            </li>
          
           
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ route('contato') }}">Contato</a>

            </li>

            <li style="color:#fff; padding: 2px;" class="nav-item">
               <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents">
              <i class="material-icons">account_circle</i>&nbsp; <?php $nome = explode(" ", Auth()->user()->name); echo $nome[0]; ?>
               </a>
                <ul class="sidenav-second-level collapse" id="collapseComponents">
                  <li><a href="{{ route('home') }}">Entrar</a></li>
                  <li><a href="{{ route('meu_perfil') }}">Perfil</a></li>
                  <li> <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">Sair</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form></li>     
                </ul>
              </li>

          </ul>
        </div>
      </div>
    </nav>
    @endif
  <br><br><br><br>
  @if(Session::has('flash_message_error'))
  <div class="text-center alert alert-danger"><span  class="text-center glyphicon glyphicon-ok"></span><em> {!! session('flash_message_error') !!}</em></div>
  @endif
  @if(Session::has('flash_message_sucess'))
  <div class="text-center alert alert-success"><span  class="text-center glyphicon glyphicon-ok"></span><em> {!! session('flash_message_sucess') !!}</em></div>
  @endif
  <section id="visualizar">
    <!-- Page Content -->
    <div class="container">
      <h1 class="my-4">Detalhes da vaga

      </h1>

      <div class="row">


        <div class="col-md-4">
          @foreach ($trabs as $trab)
          <div class="card my-4">
            <h5 class="card-header">Informações adicionais</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <ul class="list-unstyled">
                    
                    <li>
                      <p><i class="fa fa-hourglass-half" aria-hidden="true"></i> {{$trab->carga_horaria}} horas semanais</p>
                    </li>
                    <li>
                      <p><i class="fa fa-map-marker" aria-hidden="true"></i> {{$trab->bairro}} - {{$trab->cidade}} - {{$trab->estado}}</p>
                    </li>
                    <li>
                      @if($trab->salario == 0)
                        <p><i class="fa fa-handshake-o" aria-hidden="true"></i> Salário à combinar</p>
                      @else
                        <p><i class="fa fa-handshake-o" aria-hidden="true"></i> R${{$trab->salario}}</p>
                      @endif
                    </li>
                  </ul>
                </div>

              </div>
            </div>
          </div>

          <div class="card my-4">
            <h5 class="card-header">Importante</h5>
            <div class="card-body">
              1) Antes de candidatar-se, saiba o que pensam os outros usuários sobre o contratante através do seu perfil;<br><br>
              2) Mantenha sempre os dados atualizados, assim fica mais fácil conseguir um emprego;

            </div>
          </div>

        </div>

        <div class="col-md-8">
          <br>
          <div class="card mb-4">

            <div class="card-body">
              <h3>Contratante:</h3> <p>{{$trab->nome_contratante}}</p>
              <h3>Cargo:</h3> <p>{{$trab->cargo}}</p>
              <h3>Benefícios:</h3>
              <p class="card-text">{{$trab->beneficios}}</p>
              <h3>Descrição:</h3>
              <p class="card-text">{{$trab->descricao}}</p>
              <a href="{{ route('candidatar',['id2'=>encrypt($trab->id_usuario),'id3'=>encrypt($trab->id)])}}" onclick="javascript:if(!confirm('Deseja se candidatar??'))return false;"class="btn btn-primary">Candidatar</a>
            </div>
            <div class="card-footer text-muted">
              Postado em {{$trab->created_at}}

            </div>
          </div>

        </div>

      </div>
      <!-- /.row -->

    </div>
    @endforeach

</div>

</section>


<!-- Footer -->
<footer class="" style="background-color:#000;">
  <div class=" text-center">
    <p style="color:#fff;">Copyright &copy; seutrabalhoaqui 2017</p>
  </div>
</footer>
<!-- Bootstrap core JavaScript -->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/popper/popper.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>

<!-- Custom scripts for this template -->
<script src="/js/sta.min.js"></script>

</body>


</html>
