<?php
use App\Trabalho;
use App\Mail\KryptoniteFound;



/* ROTAS SESSION*/
Route::get('session/get','SessionController@accessSessionData');
Route::get('session/set','SessionController@storeSessionData');
Route::get('session/remove','SessionController@deleteSessionData');

/* ROTAS LOGIN/LOGOUT*/
Route::get('/login', ['as' =>'login', 'uses' => 'Auth\LoginController@getLogin']);
Route::post('login', ['as' =>'login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

/*Rota principal*/
Route::get('/', 'HomeController@index')->name('index');

Auth::routes();

/*Rotas curriculo*/
Route::any('download_curriculo/{id}/',['as' => 'download_curriculo', 'uses' => 'TrabalhoController@download_curriculo']);

Route::post('editar_c/{id}/',['as' => 'editar_c', 'uses' => 'TrabalhoController@update']);
Route::any('meucurriculo/',['as' => 'meucurriculo', 'uses' => 'TrabalhoController@createpdf']);
Route::any('visualizar_curriculo/{id}',['as' => 'visualizar_curriculo', 'uses' => 'TrabalhoController@visualizar_curriculo']);
Route::get('excluir_expr/{id2}/',['as' => 'excluir_expr', 'uses' => 'TrabalhoController@excluir_expr']);
Route::get('excluir_form/{id2}/',['as' => 'excluir_form', 'uses' => 'TrabalhoController@excluir_form']);
Route::get('dados_pessoais_emp/',['as' => 'dados_pessoais_emp', 'uses' => 'TrabalhoController@dados_pessoais_emp']);
Route::get('experiencia_emprego/',['as' => 'experiencia_emprego', 'uses' => 'TrabalhoController@experiencia_emprego']);
Route::get('formacao_emprego/',['as' => 'formacao_emprego', 'uses' => 'TrabalhoController@formacao_emprego']);
Route::post('addexpr/',['as' => 'addexpr', 'uses' => 'TrabalhoController@addexpr']);
Route::post('addform/',['as' => 'addform', 'uses' => 'TrabalhoController@addform']);
Route::post('addpessoais/',['as' => 'addpessoais', 'uses' => 'TrabalhoController@addpessoais']);
Route::get('editar_curriculo/',['as' => 'editar_curriculo', 'uses' => 'TrabalhoController@editar']);

/*Rotas sistemas*/
Route::get('ver_vaga/{id}/',['as' => 'ver_vaga', 'uses' => 'HomeController@visualizar']);
Route::get('contato',['as' => 'contato', 'uses' => 'HomeController@contato']);
Route::any('envio',['as' => 'envio', 'uses' => 'HomeController@envio']);
Route::post('search_index/',['as' => 'search_index', 'uses' => 'HomeController@search_index']);

/*Rotas de perfil*/
Route::get('perfil/{id}/',['as' => 'perfil', 'uses' => 'UserController@perfil']);
Route::get('meu_perfil',['as' => 'meu_perfil', 'uses' => 'UserController@meu_perfil']);
Route::get('editar_perfil/',['as' => 'editar_perfil', 'uses' => 'UserController@editar_perfil']);
Route::get('denunciar_usuario/{id}',['as' => 'denunciar_usuario', 'uses' => 'UserController@denunciar_usuario']);
Route::post('update_perfil/',['as' => 'update_perfil', 'uses' => 'UserController@update_perfil']);

/*Rotas trabalho*/
Route::any('pesquisar_emp/',['as' => 'pesquisar_emp', 'uses' => 'HomeController@pesquisar_emp']);
Route::get('/ver_vagas_empregos', 'EmpregoController@ver_vagas_empregos')->name('ver_vagas_empregos');
Route::get('visualizarvaga', 'TrabalhoController@visualizarvaga')->name('visualizarvaga');
Route::get('listar_emp',['as' => 'listar_emp', 'uses' => 'HomeController@listar']);
Route::any('candidatar/{id2}/{id3}/',['as' => 'candidatar', 'uses' => 'TrabalhoController@candidatar']);
Route::post('/cadastro_oportunidade', 'UserController@cadastro_oportunidade')->name('cadastro_oportunidade');
Route::get('/divulgar_emprego', 'UserController@divulgar_emprego')->name('divulgar_emprego');
Route::get('vagas_ofertadas', 'TrabalhoController@vagas_ofertadas')->name('vagas_ofertadas');
Route::get('candidatos/{id}/', 'TrabalhoController@candidatos')->name('candidatos');
Route::post('pesquisar_vaga_ofer', 'TrabalhoController@pesquisar_vaga_ofer')->name('pesquisar_vaga_ofer');
Route::get('editar_vaga_ofer/{id}', 'TrabalhoController@editar_vaga_ofer')->name('editar_vaga_ofer');
Route::post('update_vaga_ofer/{id}', 'TrabalhoController@update_vaga_ofer')->name('update_vaga_ofer');
Route::get('excluir_vaga_ofer/{id}', 'TrabalhoController@excluir_vaga_ofer')->name('excluir_vaga_ofer');
Route::auth();Route::auth();Route::auth();Route::auth();Route::auth();Route::auth();
Route::get('approve/{id}/{id2}/', 'TrabalhoController@approve_candidate')->name('approve');
Route::get('disapprove/{id}/{id2}/', 'TrabalhoController@disapprove_candidate')->name('disapprove');
/*Rotas admin*/
Route::middleware(['auth', 'auth.admin'])->group(function () {
    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::get('listar_experiencia_emprego', 'AdminController@listar_experiencia_emp')->name('listar_experiencia_emprego');
    Route::get('listar_formacao_emp', 'AdminController@listar_formacao_emp')->name('listar_formacao_emp');
    Route::get('listar_vagas_emp', 'AdminController@listar_vagas')->name('listar_vagas_emp');
    Route::get('editar_vaga_emp/{id}', 'AdminController@editar_vagas_emp')->name('editar_vaga_emp');
    Route::get('excluir_vaga_emp/{id}', 'AdminController@excluir_vaga_emp')->name('excluir_vaga_emp');
    Route::post('pesquisar_usuario', 'AdminController@pesquisar_usuario')->name('pesquisar_usuario');
    Route::post('pesquisar_vaga_emp', 'AdminController@pesquisar_vaga_emp')->name('pesquisar_vaga_emp');
    Route::post('update_vaga_emp/{id}', 'AdminController@update_vagas_emp')->name('update_vaga_emp');
    Route::get('editar_usuario/{id}', 'AdminController@editar_usuario')->name('editar_usuario');
    Route::post('update_usuario/{id}', 'AdminController@update_usuario')->name('update_usuario');
    Route::get('excluir_usuario/{id}', 'AdminController@excluir_usuario')->name('excluir_usuario');
    Route::get('listar_usuarios', 'AdminController@listar_usuarios')->name('listar_usuarios');
    Route::get('listar_dados_pessoais_emp', 'AdminController@listar_dados_pessoais_emp')->name('listar_dados_pessoais_emp');
});


Auth::routes();
Route::get('/home', 'HomeController@index2')->name('home');
