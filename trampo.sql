-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 24-Mar-2018 às 03:44
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trampo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `candidatura`
--

CREATE TABLE `candidatura` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `id_divulgar` int(10) UNSIGNED NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pendente',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `candidatura`
--

INSERT INTO `candidatura` (`id`, `id_usuario`, `id_divulgar`, `score`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, 'Aprovado', '2018-03-24 05:05:50', '2018-03-24 05:05:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `curriculo_emprego`
--

CREATE TABLE `curriculo_emprego` (
  `id` int(10) UNSIGNED NOT NULL,
  `naturalidade` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idade` int(11) NOT NULL,
  `endereco` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `objetivo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qualificacoes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info_add` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `curriculo_emprego`
--

INSERT INTO `curriculo_emprego` (`id`, `naturalidade`, `idade`, `endereco`, `celular`, `objetivo`, `qualificacoes`, `info_add`, `id_usuario`, `created_at`, `updated_at`) VALUES
(1, 'Brasileiro', 23, 'Parque Recreio dos Bandeirantes quadra 10 lote 3A', '71991890688', 'Estágio em desenvolvimento web', 'ggfgf', 'gfgf', 1, '2018-03-24 05:01:15', '2018-03-24 05:01:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `divulgar_emprego`
--

CREATE TABLE `divulgar_emprego` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome_contratante` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_cont` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_empresa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `turno` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carga_horaria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salario` double NOT NULL DEFAULT '0',
  `beneficios` varchar(1800) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pre_requisitos` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Ativo',
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `divulgar_emprego`
--

INSERT INTO `divulgar_emprego` (`id`, `nome_contratante`, `tipo_cont`, `email_empresa`, `cargo`, `turno`, `carga_horaria`, `bairro`, `cidade`, `estado`, `salario`, `beneficios`, `pre_requisitos`, `descricao`, `status`, `id_usuario`, `created_at`, `updated_at`) VALUES
(1, 'teste', 'clt', 'dcarvaioo@gmail.com', 'teste', 'vespertino', '2', 'teste', 'teste', 'Piauí', 0, NULL, NULL, 'tretrtr', 'Ativo', 1, '2018-03-24 04:17:06', '2018-03-24 04:17:06'),
(2, 'CTI Tecnologia', 'clt', 'dcarvaioo@gmail.com', 'Desenvolvedor Java Jr', 'flexivel', '20', 'Tocantins', 'Salvador', 'Mato Grosso do Sul', 0, NULL, NULL, 'reeeeeeeeeee', 'Ativo', 1, '2018-03-24 04:50:28', '2018-03-24 04:50:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE `empresa` (
  `cnpj` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estagio`
--

CREATE TABLE `estagio` (
  `id` int(10) UNSIGNED NOT NULL,
  `semestre_i` int(11) NOT NULL,
  `semestre_f` int(11) NOT NULL,
  `formacao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_vaga` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `experiencias`
--

CREATE TABLE `experiencias` (
  `id` int(10) UNSIGNED NOT NULL,
  `empresa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `periodo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_atv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_curriculo_emp` int(10) UNSIGNED NOT NULL,
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `formacao`
--

CREATE TABLE `formacao` (
  `id` int(10) UNSIGNED NOT NULL,
  `escolaridade` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instituicao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `curso` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semestre` int(11) NOT NULL,
  `previsao` int(11) NOT NULL,
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `formacao`
--

INSERT INTO `formacao` (`id`, `escolaridade`, `instituicao`, `curso`, `semestre`, `previsao`, `id_usuario`, `created_at`, `updated_at`) VALUES
(1, 'superior_incompleto', 'Universidade Federal da Bahia', 'CC', 11, 2019, 1, '2018-03-24 05:05:23', '2018-03-24 05:05:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_06_07_102023_create_addcolumn_users_table', 1),
(2, '2018_03_18_235547_create_pessoa_fisica_table', 1),
(3, '2018_03_18_235614_create_empresa_table', 1),
(4, '2017_06_09_223711_create_table_divulgar_emprego', 2),
(5, '2017_09_21_234222_create_candidatura_table', 2),
(6, '2017_09_23_024348_create_curriculo_trabalho_table', 2),
(7, '2017_09_23_024659_create_experiencias_trabalho_table', 2),
(8, '2017_09_23_024930_create_formacao_table', 2),
(9, '2018_03_06_224001_create_table_estagio', 2),
(10, '2018_03_06_224029_create_table_temporario', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `temporario`
--

CREATE TABLE `temporario` (
  `id` int(10) UNSIGNED NOT NULL,
  `duracao` int(11) NOT NULL,
  `id_vaga` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bairro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `bairro`, `cidade`, `estado`, `pais`, `cpf`, `sexo`) VALUES
(1, 'Tiago Dória', 'tiagodoriap@gmail.com', '$2y$10$G3ST627opBpJJwLxS9j9auzH835j5CM.SvowzznofG7wGFlPcpV6y', 'zP63qkj4gXX59U09Avm7CwqOjzUopAuA8pkR437TaBWD9uOrnyKZ9ydc9Vb6', '2018-03-19 03:23:16', '2018-03-19 03:23:16', 'São Cristovao', 'Salvador', 'Bahia', 'Brasil', '06636203501', 'Masculino');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidatura`
--
ALTER TABLE `candidatura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `candidatura_id_usuario_foreign` (`id_usuario`),
  ADD KEY `candidatura_id_divulgar_foreign` (`id_divulgar`);

--
-- Indexes for table `curriculo_emprego`
--
ALTER TABLE `curriculo_emprego`
  ADD PRIMARY KEY (`id`),
  ADD KEY `curriculo_emprego_id_usuario_foreign` (`id_usuario`);

--
-- Indexes for table `divulgar_emprego`
--
ALTER TABLE `divulgar_emprego`
  ADD PRIMARY KEY (`id`),
  ADD KEY `divulgar_emprego_id_usuario_foreign` (`id_usuario`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD KEY `empresa_id_user_foreign` (`id_user`);

--
-- Indexes for table `estagio`
--
ALTER TABLE `estagio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `estagio_id_vaga_foreign` (`id_vaga`);

--
-- Indexes for table `experiencias`
--
ALTER TABLE `experiencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `experiencias_id_curriculo_emp_foreign` (`id_curriculo_emp`),
  ADD KEY `experiencias_id_usuario_foreign` (`id_usuario`);

--
-- Indexes for table `formacao`
--
ALTER TABLE `formacao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formacao_id_usuario_foreign` (`id_usuario`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporario`
--
ALTER TABLE `temporario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `temporario_id_vaga_foreign` (`id_vaga`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidatura`
--
ALTER TABLE `candidatura`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `curriculo_emprego`
--
ALTER TABLE `curriculo_emprego`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `divulgar_emprego`
--
ALTER TABLE `divulgar_emprego`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `estagio`
--
ALTER TABLE `estagio`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `experiencias`
--
ALTER TABLE `experiencias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `formacao`
--
ALTER TABLE `formacao`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `temporario`
--
ALTER TABLE `temporario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `candidatura`
--
ALTER TABLE `candidatura`
  ADD CONSTRAINT `candidatura_id_divulgar_foreign` FOREIGN KEY (`id_divulgar`) REFERENCES `divulgar_emprego` (`id`),
  ADD CONSTRAINT `candidatura_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `curriculo_emprego`
--
ALTER TABLE `curriculo_emprego`
  ADD CONSTRAINT `curriculo_emprego_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `divulgar_emprego`
--
ALTER TABLE `divulgar_emprego`
  ADD CONSTRAINT `divulgar_emprego_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `empresa`
--
ALTER TABLE `empresa`
  ADD CONSTRAINT `empresa_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `estagio`
--
ALTER TABLE `estagio`
  ADD CONSTRAINT `estagio_id_vaga_foreign` FOREIGN KEY (`id_vaga`) REFERENCES `divulgar_emprego` (`id`);

--
-- Limitadores para a tabela `experiencias`
--
ALTER TABLE `experiencias`
  ADD CONSTRAINT `experiencias_id_curriculo_emp_foreign` FOREIGN KEY (`id_curriculo_emp`) REFERENCES `curriculo_emprego` (`id`),
  ADD CONSTRAINT `experiencias_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `formacao`
--
ALTER TABLE `formacao`
  ADD CONSTRAINT `formacao_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `temporario`
--
ALTER TABLE `temporario`
  ADD CONSTRAINT `temporario_id_vaga_foreign` FOREIGN KEY (`id_vaga`) REFERENCES `divulgar_emprego` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
